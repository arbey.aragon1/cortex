import dash
import dash_core_components as dcc
import dash_admin_components as dac
import dash_html_components as html
import dash
import dash_table

data = []
columns = [{'id': c, 'name': v} for c, v in zip(
    ['date', 'type', 'msg'], ['Fecha', 'Tipo', 'Mensaje'])]

warning_tab = dac.TabItem(id='content_warning',

                          children=html.Div(
                              [
                                  dash_table.DataTable(

                                      data=data,
                                      columns=columns,

                                      style_cell_conditional=[
                                          {
                                              'if': {'column_id': c},
                                              'textAlign': 'left'
                                          } for c in ['Date', 'Region']
                                      ],
                                      style_data_conditional=[
                                          {
                                              'if': {'row_index': 'odd'},
                                              'backgroundColor': 'rgb(248, 248, 248)'
                                          }
                                      ],
                                      style_header={
                                          'backgroundColor': 'rgb(230, 230, 230)',
                                          'fontWeight': 'bold',
                                      },
                                      merge_duplicate_headers=True,
                                  )
                              ],
                              className='row'
                          )
                          )
