import numpy as np
import pandas as pd
import joblib
import os
import warnings
from sklearn.linear_model import LinearRegression
from .predictor_interface import PredictorInterface
from .shared.env_var_service import EnvVarService

class LinearRegressionPredictor(PredictorInterface):
    __instance = None
    __envVarService = None

    __model = None
    __config = None
    
    def __init__(self, envVarService: EnvVarService):
        self.__envVarService = envVarService

    @staticmethod
    def getModelName():
        return 'LinearRegressionPredictor'

    @staticmethod
    def getInstance(envVarService: EnvVarService):
        if LinearRegressionPredictor.__instance == None:
            LinearRegressionPredictor.__instance = LinearRegressionPredictor(envVarService)
        return LinearRegressionPredictor.__instance
    
    @staticmethod
    def getDefaultParams():
        return {
            'epochs': 10
        }

    def setParams(self, config):
        self.__config = config

    def createModel(self):
        self.__modelo = LinearRegression()

    def fit(self, Xtrain, Ytrain, Xtest=None, Ytest=None):
        Xtrain = np.reshape(Xtrain.values, (Xtrain.shape[0], Xtrain.shape[1]))
        Ytrain = np.reshape(Ytrain.values, (Ytrain.shape[0], Ytrain.shape[1]))
        self.__modelo.fit(Xtrain, Ytrain)

    def predict(self, Xtest):
        Xtest = np.reshape(Xtest.values, (Xtest.shape[0], Xtest.shape[1]))
        yPredict = self.__modelo.predict(Xtest)
        return yPredict

    def saveModel(self, path = None):
        if(path == None):
            path = os.path.join(self.__envVarService.PATH_MODELS, "model"+LinearRegressionPredictor.getModelName()+".pkl")
        joblib.dump(self.__modelo, path) 

    def loadModel(self, path = None):
        if(path == None):
            path = os.path.join(self.__envVarService.PATH_MODELS, "model"+LinearRegressionPredictor.getModelName()+".pkl")
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=UserWarning)
            self.__modelo = joblib.load(path)