import numpy as np
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
class ErrorMetrics():
    @staticmethod
    def __calculateMape(y_true, y_pred):
        """ Calculate mean absolute percentage error (MAPE)"""
        return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

    @staticmethod
    def __calculateMpe(y_true, y_pred):
        """ Calculate mean percentage error (MPE)"""
        return np.mean((y_true - y_pred) / y_true) * 100

    @staticmethod
    def __calculateMae(y_true, y_pred):
        """ Calculate mean absolute error (MAE)"""
        return np.mean(np.abs(y_true - y_pred))

    @staticmethod
    def __calculateMse(y_true, y_pred):
        """ Calculate mean absolute error (MSE)"""
        return np.mean((y_true - y_pred)**2)

    @staticmethod
    def __calculateRmse(y_true, y_pred):
        """ Calculate root mean square error (RMSE)"""
        return np.sqrt(np.mean((y_true - y_pred)**2))

    @staticmethod
    def __calculateR2(y_true, y_pred):
        """ Calculate R2 (R2)"""
        y_mu = np.mean(y_true)
        return 1-(np.mean((y_true - y_pred)**2))/(np.mean((y_true - y_mu)**2))

    @staticmethod
    def generateMetrics(y_true, y_pred, scalar = False):
        if(not scalar):
            return {
                'MAPE': ErrorMetrics.__calculateMape(y_true, y_pred).to_dict(),
                'MPE': ErrorMetrics.__calculateMpe(y_true, y_pred).to_dict(),
                'MAE': ErrorMetrics.__calculateMae(y_true, y_pred).to_dict(),
                'MSE': ErrorMetrics.__calculateMse(y_true, y_pred).to_dict(),
                'RMSE': ErrorMetrics.__calculateRmse(y_true, y_pred).to_dict(),
                'R2': ErrorMetrics.__calculateR2(y_true, y_pred).to_dict(),
            }
        else:
            return {
                'MAPE': ErrorMetrics.__calculateMape(y_true, y_pred),
                'MPE': ErrorMetrics.__calculateMpe(y_true, y_pred),
                'MAE': ErrorMetrics.__calculateMae(y_true, y_pred),
                'MSE': ErrorMetrics.__calculateMse(y_true, y_pred),
                'RMSE': ErrorMetrics.__calculateRmse(y_true, y_pred),
                'R2': ErrorMetrics.__calculateR2(y_true, y_pred),
            }
        
    @staticmethod
    def printErrorMetrics(d):
        for k in d.keys():
            print(f'{k}: {d[k]}')