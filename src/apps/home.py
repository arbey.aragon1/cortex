import dash
import dash_core_components as dcc
import dash_admin_components as dac
import dash_html_components as html
app = dash.Dash(__name__)

def home():
    app.layout = html.Div(html.Img(src=app.get_asset_url('logo_home.png'), style={'height': '70%', 'width': '70%'}))
    return app.layout

home_tab = dac.TabItem(id='content_home', 
                                                  
    children=html.Div(
        [
          home()
        ], style={'textAlign': 'center'}
                  
    )
    
)

