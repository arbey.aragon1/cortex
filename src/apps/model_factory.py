from .linear_regression_predictor import LinearRegressionPredictor
from .lstm_predictor import LSTMPredictor
from .svr_predictor import SVRPredictor
from .rnn_predictor import RNNPredictor
from .random_forest_regressor_predictor import RandomForestRegressorPredictor
from .shared.env_var_service import EnvVarService

class ModelFactory(object):
    @staticmethod
    def getInstance(envVarService: EnvVarService, key):
        if LinearRegressionPredictor.getModelName() == key:
            instance = LinearRegressionPredictor.getInstance(envVarService)
        elif RNNPredictor.getModelName() == key:
            instance = RNNPredictor.getInstance(envVarService)
        elif LSTMPredictor.getModelName() == key:
            instance = LSTMPredictor.getInstance(envVarService)
        elif LinearRegressionPredictor.getModelName() == key:
            instance = LinearRegressionPredictor.getInstance(envVarService)
        elif RandomForestRegressorPredictor.getModelName() == key:
            instance = RandomForestRegressorPredictor.getInstance(envVarService) 
        elif SVRPredictor.getModelName() == key:
            instance = SVRPredictor.getInstance(envVarService) 
        try:
            from .xgb_regressor_predictor import XGBRegressorPredictor
            if XGBRegressorPredictor.getModelName() == key:
                instance = XGBRegressorPredictor.getInstance(envVarService)    
        except Exception:
            print('ModelFactory :: Error in XGBRegressorPredictor load')
        return  instance
            