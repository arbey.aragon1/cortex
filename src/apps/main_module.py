import os
import tempfile
from .firestore_service import FirestoreService
from .shared.db_service import DBService
from .file_manager_controller import FileManagerController
from .module1_controller import Module1Controller
from .module2_controller import Module2Controller
from .data_warehouse_controller import DataWarehouseController, BalanceManagerController
from .shared.location_service import LocationService
from .shared.env_var_service import EnvVarService

from .feature_engineering import FeatureEngineering
from .linear_regression_predictor import LinearRegressionPredictor
from .predictor_service import PredictorService
from .scaler_service  import ScalerService

class MainModule():
    __instance = None
    __loadDB = True

    def __init__(self):
        self.__loadModules()

    @staticmethod
    def getInstance():
        if MainModule.__instance == None:
            MainModule.__instance = MainModule()
        return MainModule.__instance

    def __loadModules(self):
        self.clearDB()
        
        self.__envVarService = EnvVarService.getInstance()
        self.__fileService = FirestoreService.getInstance(self.__envVarService)
        self.__dbService = DBService.getInstance()

        self.__fileManagerController = FileManagerController.getInstance(self.__fileService, self.__dbService)

        self.loadDB()
        
        self.__locationService = LocationService.getInstance(self.__envVarService, self.__dbService)
        self.__dataWarehouseController = DataWarehouseController.getInstance(
            self.__fileService, 
            self.__dbService, 
            self.__locationService
        )

        self.__balanceManagerController = BalanceManagerController.getInstance(self.__dataWarehouseController)

        self.__featureEngineering = FeatureEngineering.getInstance(self.__dataWarehouseController, self.__envVarService)

        self.__scalerService = ScalerService.getInstance(self.__envVarService)

        self.__predictorService = PredictorService.getInstance(self.__featureEngineering, self.__envVarService, self.__scalerService)

        self.__module1Controller = Module1Controller.getInstance(self.__balanceManagerController, self.__envVarService)
        self.__module2Controller = Module2Controller.getInstance(self.__balanceManagerController, self.__predictorService)


        
    def clearDB(self):
        if(not self.__loadDB): 
            return None
        else:
            print('Remove DB')
            path = os.path.join(tempfile.gettempdir(), "db.json")
            f = open(path, 'w')
            f.write("")
            f.close()

    def loadDB(self):
        if(not self.__loadDB): 
            return None
        else:
            print('Load DB')
            files = [
                ['BEEMMODA.xlsx','239d9f5c-f97e-11ea-82c9-181deaf44bd2'],
                ['CRISTIANDRADE.xlsx','2ad5cb1a-f97e-11ea-9ed2-181deaf44bd2'],
                ['DIANASANCHEZ.xlsx','2b364198-f97e-11ea-8783-181deaf44bd2'],
                ['EL UNIVERSO.xlsx','2ba21db4-f97e-11ea-bc11-181deaf44bd2'],
                ['FAMOKA11.xlsx','2c05dfee-f97e-11ea-9164-181deaf44bd2'],
                ['GOTAGUA.xlsx','2c7fa4b0-f97e-11ea-b00f-181deaf44bd2'],
                ['ISAZAJOHAN.xlsx','2cf0fb36-f97e-11ea-9a30-181deaf44bd2'],
                ['LABODEGA.xlsx','2d5cce50-f97e-11ea-9c7c-181deaf44bd2'],
                ['SOCIEDAD.xlsx','2dcc92d2-f97e-11ea-a7bd-181deaf44bd2'],
                ['UNITED.xlsx','2e419f76-f97e-11ea-915f-181deaf44bd2'],
                ['WILLIAMROJAS.xlsx','2e8f6f28-f97e-11ea-8ed7-181deaf44bd2'],
                ['WILSONARISTIZABAL.xlsx','2eed8c26-f97e-11ea-8ad0-181deaf44bd2']
                ]
            for f in files:
                self.__fileManagerController.loadAndSaveFile(f[1], f[0])
        

    def EnvVarService(self):
        return self.__envVarService

    def FileService(self):
        return self.__fileService

    def DBService(self):
        return self.__fileService

    def FileManagerController(self):
        return self.__fileManagerController
        
    def DataWarehouseController(self):
        return self.__dataWarehouseController
        
    def Module1Controller(self):
        return self.__module1Controller
    
    def Module2Controller(self):
        return self.__module2Controller

    def LocationService(self):
        return self.__locationService

    def FeatureEngineering(self):
        return self.__featureEngineering

    def ScalerService(self):
        return self.__scalerService

    def PredictionService(self):
        return self.__predictorService
