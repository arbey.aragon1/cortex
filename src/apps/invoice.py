import dash_html_components as html
import dash_admin_components as dac
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import pandas as pd
import re
import dash_core_components as dcc

from .main_module import MainModule

mainModule = MainModule.getInstance()
dataWarehouseController = mainModule.DataWarehouseController()

fileData = dataWarehouseController.getFilesData()
fileNames = [fileData[i]['fileName'] for i in fileData.keys()]

def invoices():
        data = [go.Table(
                header=dict(values=list(fileData[k]['data'].columns),
                                fill_color='paleturquoise',
                                align='left'),
                cells=dict(values=[fileData[k]['data'][i] for i in fileData[k]['data'].columns],
                        fill_color='lavender',
                        align='left')
                ) for k in fileData.keys()]

        updatemenus = list([
                dict(active=-1,
                        buttons=list([   
                                dict(label = fileData[k]['fileName'],
                                method = "update",
                                args = [{"visible": [n == fileData[k]['fileName'] for n in fileNames]},
                                        {"title": fileData[k]['fileName']}])
                                for k in fileData.keys()
                        ]),
                )
        ])
        layout = dict(title="Facturas de los Clientes",
                showlegend=False,
                xaxis=dict(title=""),
                yaxis=dict(title=""),
                updatemenus=updatemenus)
        fig = dict(data=data, layout=layout)

        return fig


invoice_tab = dac.TabItem(id='content_invoice', 
                              
    children=[
      dcc.Graph(
              figure=invoices(),
              style = {'display': 'center', 'width': '1050px', 'height': '600px',  'textAlign': 'center'}
          ),       

  ]
)