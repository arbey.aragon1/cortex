from datetime import date
import uuid
import tempfile
import re
import requests
import os
import pandas as pd
import sys

from .firestore_service import FirestoreService
from .shared.db_service import DBService
from .shared.location_service import LocationService

class FileManagerController():
    __instance = None
    __fileService = None
    __dbService = None
    KEY_FILES_TABLE = 'files'

    def __init__(self, fileService: FirestoreService, dbService: DBService):
        self.__fileService = fileService
        self.__dbService = dbService

    @staticmethod
    def getInstance(fileService: FirestoreService, dbService: DBService):
        if FileManagerController.__instance == None:
            FileManagerController.__instance = FileManagerController(fileService, dbService)
        return FileManagerController.__instance

    def __generateUUID(self):
        return str(uuid.uuid1())

    def loadAndSaveFile(self, uuid: str, fileName: str):
        path = self.__fileService.downloadFile(uuid, fileName)
        data = {
            'fileName': fileName,
            'uuid': uuid,
            'date': str(date.today()),
            'path': path}
        self.__dbService.setItem(
            FileManagerController.KEY_FILES_TABLE, uuid, data)
        return data

    def getFileListData(self):
        return self.__dbService.listItems(
            FileManagerController.KEY_FILES_TABLE)

    def deleteFile(self, uuid):
        self.__dbService.deleteItem(FileManagerController.KEY_FILES_TABLE, uuid)
        self.__fileService.deleteFile(uuid)
