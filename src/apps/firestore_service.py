
import os
import sys
import requests
import re
import tempfile
import uuid
import firebase_admin
from firebase_admin import credentials, firestore, storage

from .shared.env_var_service import EnvVarService

class FirestoreService():
    __instance = None
    __envVarService = None
    __bucket = None
    
    def __init__(self, envVarService: EnvVarService):
        self.__envVarService = envVarService
        cred = credentials.Certificate(self.__envVarService.PATH_CREDENTIALS+'/serviceAccountKey.json')
        firebase_admin.initialize_app(cred, {
            'storageBucket': 'halogen-brace-266119.appspot.com'
        })
        db = firestore.client()
        self.__bucket = storage.bucket()

    @staticmethod
    def getInstance(envVarService: EnvVarService):
        if FirestoreService.__instance == None:
            FirestoreService.__instance = FirestoreService(envVarService)
        return FirestoreService.__instance

    def uploadFile(self, localFolder, key, fileName):
        print('uploadFile')
        #localFolder = os.path.join(tempfile.gettempdir(), key)
        self.__createFolder(localFolder)
        blob = self.__bucket.blob(key+'/'+fileName)
        pathFile = os.path.join(localFolder, fileName)#'C:/Users/Oscar/Documents/cortex/README.md'
        with open(pathFile, 'rb') as file:
            blob.upload_from_file(file)
        return pathFile

    def downloadFile(self, key, fileName):
        print('downloadFile')
        localFolder = os.path.join(tempfile.gettempdir(), key)
        self.__createFolder(localFolder)
        blob = self.__bucket.blob(key+'/'+fileName)
        fileName = blob.name.split('/')[-1]
        path = os.path.join(localFolder, fileName)
        blob.download_to_filename(path)
        return path

    def deleteFile(self, key):
        blob = self.__bucket.blob(key)
        blob.delete()
        path = os.path.join(tempfile.gettempdir(), key)
        try:
            os.rmdir(path)
        except OSError as e:
            print("Error: %s : %s" % (path, e.strerror))


    def __createFolder(self, localFolder):
        access_rights = 0o755
        try:
            os.mkdir(localFolder, access_rights)
        except OSError:
            print("Creation of the directory %s failed" % localFolder)
        else:
            print("Successfully created the directory %s" % localFolder)
