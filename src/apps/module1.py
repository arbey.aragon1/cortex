from plotly.subplots import make_subplots
import plotly.graph_objects as go
import dash_html_components as html
import dash_core_components as dcc
import dash_admin_components as dac
import plotly.graph_objects as go
import plotly.express as px
import pandas as pd
import json
import dash
from dash.dependencies import Input, Output
import plotly.express as px

from .main_module import MainModule

mainModule = MainModule.getInstance()
module1Controller = mainModule.Module1Controller()


module1_tab = dac.TabItem(id='content_module1', 

    children = module1Controller.viewModule(),  
)
    