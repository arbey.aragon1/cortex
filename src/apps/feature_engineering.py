import numpy as np
import pandas as pd
import calendar
import datetime
from .data_warehouse_controller import DataWarehouseController
from .shared.env_var_service import EnvVarService

class FeatureEngineering():
    __instance = None
    __dataWarehouseController = None
    __envVarService = None

    __valueChargeBins = [
      {'min':-np.Inf, 'max': 15e6},
      {'min':15e6, 'max': 30e6},
      {'min':30e6, 'max': 60e6},
      {'min':60e6, 'max': np.Inf}
    ]

    __valuePaymentBins = [
      {'min':-np.Inf, 'max': 10e6},
      {'min':10e6, 'max': 20e6},
      {'min':20e6, 'max': 50e6},
      {'min':50e6, 'max': np.Inf}
    ]

    __timeSinceStartBins = [
      {'min':-np.Inf, 'max': 200},
      {'min':200, 'max': 400},
      {'min':400, 'max': 600},
      {'min':600, 'max': 800},
      {'min':800, 'max': np.Inf}
    ]

    __diffBins = [
      {'min':-np.Inf, 'max': 0},
      {'min':0, 'max': 100},
      {'min':100, 'max': 300},
      {'min':300, 'max': 500},
      {'min':500, 'max': np.Inf}
    ]
    __loopBack = 3
    __loopBackCols = '_lb_'
      
    def __init__(self, dataWarehouseController: DataWarehouseController, envVarService: EnvVarService):
        self.__dataWarehouseController = dataWarehouseController
        self.__envVarService = envVarService
        self.processData()

    @staticmethod
    def getInstance(dataWarehouseController: DataWarehouseController, envVarService: EnvVarService):
        if FeatureEngineering.__instance == None:
            FeatureEngineering.__instance = FeatureEngineering(dataWarehouseController, envVarService)
        return FeatureEngineering.__instance

    def processData(self):
        self.__invoiceProfiles = self.__dataWarehouseController.getDataProfiles()
        self.__generateFillIndex()
        self.__fillNanDiff()
        self.__generateRawFeatures()
        self.__generateLogFeatures()
        self.__generateLookBackFeatures()
        self.__generateTargets()
        self.__df = self.__dfRaw
   
    def __rnan(self, v,r=0):
        if (np.isnan(v)):
            return r
        else:
            return v

    def __mapDateT(self, dt):
        return dt.apply(lambda s: str(s.year).zfill(4)+'T'+str(s.month).zfill(2))

    def __generateFillIndex(self):
        self.__fillIndex = pd.date_range(start=self.__invoiceProfiles['fecha'].min(), end=self.__invoiceProfiles['fecha'].max())
        self.__fillIndex = self.__mapDateT(self.__fillIndex.to_series()).unique()

    def __fillNanDiff(self):
        self.__invoiceProfiles['T'] = self.__mapDateT(self.__invoiceProfiles['fecha'])
        self.__invoiceProfiles['diff'] = self.__invoiceProfiles['diff'].fillna(0)
        def getChargeValue(s):
            return self.__invoiceProfiles[self.__invoiceProfiles['numero_factura'].astype(str) == str(s)]['valor'].values[0]
        self.__invoiceProfiles['valorCargo'] = self.__invoiceProfiles['numero_cargo']\
              .apply(getChargeValue)
        
    def __featuresStatistics(self, dfTem, title):
        features = {}
        features[title+'_count'] = dfTem['valor'].mean()
        features[title+'_sum'] = dfTem['valor'].sum()
        features[title+'_count'] = dfTem['valor'].shape[0]
        features[title+'_max'] = self.__rnan(dfTem['valor'].max())
        features[title+'_min'] = self.__rnan(dfTem['valor'].min())
        return features

    def __createBins(self, values, suffix, valueBins):
        dicVal = {}
        for i, l in enumerate(valueBins):
            dicVal[suffix+'_'+str(i)] = 0 
        for i, l in enumerate(valueBins):
            for val in values:
                dicVal[suffix+'_'+str(i)] += 1 if ((l['min'] <= val) & (val < l['max'])) else 0 
        return dicVal

    def __featuresStatisticsDataframe(self, df, t, features = {}):
        dfTemCharges = df[(df['tipo'] == 'cargo')]
        features = {**self.__featuresStatistics(dfTemCharges, 'charges'), **features}
        features = {**self.__createBins(dfTemCharges['valor'].values, 'charge_bin', self.__valueChargeBins), **features}

        dfTemPayments = df[(df['tipo'] == 'abono') | (df['tipo'] == 'balanceInicial')]
        features = {**self.__featuresStatistics(dfTemPayments, 'payments'), **features}
        features = {**self.__createBins(dfTemPayments['valor'].values, 'payment_bin', self.__valuePaymentBins), **features}
        features = {**self.__createBins(dfTemPayments['timeSinceStart'].values, 'time_since_start_bin', self.__timeSinceStartBins), **features}
        features = {**self.__createBins(dfTemPayments['diff'].values, 'time_diff_bin', self.__diffBins), **features}
        
        features['month_sin'] = np.sin((int(t.split('T')[1])-1)*(2.*np.pi/12))
        features['month_cos'] = np.cos((int(t.split('T')[1])-1)*(2.*np.pi/12))

        features['index'] = int(t.replace('T',''))
        return features
    
    def __generateRawFeatures(self):
        listFeatures = []
        for t in list(self.__fillIndex):
            fet = self.__featuresStatisticsDataframe(self.__invoiceProfiles[self.__invoiceProfiles['T'] == t], t)
            listFeatures.append(fet)
        listFeatures = pd.DataFrame(listFeatures)
        listFeatures.index = listFeatures['index']
        listFeatures.sort_index() 
        del listFeatures['index']
        self.__dfRaw = listFeatures

    def __generateLogFeatures(self):
        for c in ['payments_sum', 'payments_max', 'payments_min','charges_sum', 'charges_max', 'charges_min']:            
            if(self.__dfRaw[c].min()<=0):
                self.__dfRaw[c+'_log_m'] = np.log(self.__dfRaw[c] + np.abs(self.__dfRaw[c].min())+np.finfo(float).eps)
            else:
                self.__dfRaw[c+'_log'] = np.log(self.__dfRaw[c])

    def __generateLookBackFeatures(self):
        dfTem = self.__dfRaw
        for colName in self.__dfRaw.columns:
            for i in range(1,self.__loopBack+1):
                dfTem[colName + self.__loopBackCols + str(i)] = dfTem[colName].shift(i)
        self.__dfRaw = dfTem.dropna()

    def __generateTargets(self):
        for i in range(1,7):
            self.__dfRaw['y_v_'+str(i)] = self.__dfRaw['payments_sum'].shift(-1*i)
            self.__dfRaw['y_p_'+str(i)] = self.__dfRaw['payments_sum'].shift(-1*i).apply(lambda s: [0,1][s!=0])

    def getDataRaw(self):
        return self.__dfRaw.copy()

    def getDataWithoutNan(self):
        return self.__dfRaw.copy().dropna()

    

