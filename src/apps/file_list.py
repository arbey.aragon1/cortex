import dash_core_components as dcc
import dash_admin_components as dac
import dash_html_components as html
import dash_table
import sys

from .shared.db_service import DBService
from .firestore_service import FirestoreService
from .file_manager_controller import FileManagerController
from .main_module import MainModule


mainModule = MainModule.getInstance()

fileService = mainModule.FileService()
dbService = mainModule.DBService()
fileManagerController = mainModule.FileManagerController()

data = fileManagerController.getFileListData()


data = [i['data'] for i in data]
columns = [{'id': c, 'name': v} for c, v in zip(
    ['fileName', 'uuid', 'date'], ['File Name', 'uuid', 'Date'])]
    

file_list_tab = dac.TabItem(id='content_file_list',
                            children=html.Div(
                                id="file_list",
                                children=[
                                            html.Div( 
                                                children=[
                                                    html.Div(
                                                    [
                                                        html.Div([html.P("Lista de archivos Drive")], className="subtitle"),
                                                        html.P(className="twelve columns indicator_text"),
                                                    ],
                                                    className="four columns indicator pretty_container",
                                                    ),
                                                    
                                                ],
                                            ),

                                            html.Div( 
                                                children=[
                                                    html.Div(
                                                    [
                                                        dash_table.DataTable(

                                                            data=data,
                                                            columns=columns,

                                                            style_cell_conditional=[
                                                                {
                                                                    'if': {'column_id': c},
                                                                    'textAlign': 'center'
                                                                } for c in ['Date', 'Region']
                                                            ],
                                                            style_data_conditional=[
                                                                {
                                                                    'if': {'row_index': 'odd'},
                                                                    'backgroundColor': '#eff0f4'
                                                                }
                                                            ],
                                                            style_header={
                                                                'backgroundColor': '#74dbef',
                                                                'fontWeight': 'bold',
                                                            },
                                                            merge_duplicate_headers=True,
                                                        )

                                                    ],
                                                    className="four columns indicator pretty_container",
                                                    ),
                                                    
                                                ],
                                            )


                                         ]
                        
                                 )           
                            )   
            
                    
