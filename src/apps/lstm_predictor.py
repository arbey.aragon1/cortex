import pandas as pd
import numpy as np
import os
from tensorflow import keras
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from .shared.env_var_service import EnvVarService
from .predictor_interface import PredictorInterface

class LSTMPredictor(PredictorInterface):
    __instance = None
    __envVarService = None

    __model = None
    __config = None
    
    def __init__(self, envVarService: EnvVarService):
        self.__envVarService = envVarService

    @staticmethod
    def getInstance(envVarService: EnvVarService):
        if LSTMPredictor.__instance == None:
            LSTMPredictor.__instance = LSTMPredictor(envVarService)
        return LSTMPredictor.__instance
    
    @staticmethod
    def getModelName():
        return 'LSTMPredictor'

    @staticmethod
    def getDefaultParams():
        return {
            'inputShape': None,
            'outputShape': None,
            'epochs': 50,
            'units_1': 75 
        }

    def setParams(self, config):
        self.__config = config

    def createModel(self):
        self.__modelo = Sequential()
        self.__modelo.add(LSTM(units=self.__config['units_1'], input_shape=(self.__config['inputShape'], 1)))
        self.__modelo.add(Dense(units=self.__config['outputShape']))

    def fit(self, Xtrain, Ytrain, Xtest=None, Ytest=None):
        Xtrain = np.reshape(Xtrain.values, (Xtrain.shape[0], Xtrain.shape[1], 1))
        Ytrain = np.reshape(Ytrain.values, (Ytrain.shape[0], Ytrain.shape[1]))
        Xtest = np.reshape(Xtest.values, (Xtest.shape[0], Xtest.shape[1], 1))
        Ytest = np.reshape(Ytest.values, (Ytest.shape[0], Ytest.shape[1]))
        self.__modelo.compile(optimizer='adam', loss='mse', metrics=['accuracy'])
        history = self.__modelo.fit(Xtrain, Ytrain, epochs=self.__config['epochs'], batch_size=32, validation_data=(Xtest, Ytest))
        return history

    def predict(self, Xtest):
        Xtest = np.reshape(Xtest.values, (Xtest.shape[0], Xtest.shape[1], 1))
        yPredict = self.__modelo.predict(Xtest)
        return yPredict

    def saveModel(self, path = None):
        if(path == None):
            path = os.path.join(self.__envVarService.PATH_MODELS, "model"+LSTMPredictor.getModelName()+".h5")
        ##self.__modelo.save_weights(path, overwrite=True)
        json_config = self.__modelo.to_json()
        with open(path+'.json', 'w') as json_file:
            json_file.write(json_config)
        self.__modelo.save_weights(path, overwrite=True)


    def loadModel(self, path = None):
        if(path == None):
            path = os.path.join(self.__envVarService.PATH_MODELS, "model"+LSTMPredictor.getModelName()+".h5")
        #self.__model = load_model(path)
        try:
            with open(path+'.json') as json_file:
                json_config = json_file.read()
            self.__modelo = keras.models.model_from_json(json_config)
            self.__modelo.load_weights(path)
        except Exception:
            pass  # or you could use 'continue'