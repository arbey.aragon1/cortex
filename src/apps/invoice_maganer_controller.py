class InvoiceManagerController():
    __instance = None
    
    def __init__(self):
        pass

    @staticmethod
    def getInstance():
        if InvoiceManagerController.__instance == None:
            InvoiceManagerController.__instance = InvoiceManagerController()
        return InvoiceManagerController.__instance
    