class PredictorInterface(object):
    def getModelName(self):
        raise Exception("Function not implemented!")

    def getDefaultParams(self):
        raise Exception("Function not implemented!")

    def setParams(self, config):
        raise Exception("Function not implemented!")

    def createModel(self):
        raise Exception("Function not implemented!")

    def fit(self, Xtrain, Ytrain, Xtest=None, Ytest=None):
        raise Exception("Function not implemented!")

    def predict(self, Xtest):
        raise Exception("Function not implemented!")

    def saveModel(self, path = None):
        raise Exception("Function not implemented!")
        
    def loadModel(self, path = None):
        raise Exception("Function not implemented!")
