import pandas as pd
import numpy as np
from .linear_regression_predictor import LinearRegressionPredictor
from .svr_predictor import SVRPredictor
from .lstm_predictor import LSTMPredictor

from .feature_engineering import FeatureEngineering
from .model_factory import ModelFactory
from .shared.env_var_service import EnvVarService
from .scaler_service  import ScalerService
from .time_utilities import TimeUtilities

class PredictorService():
    __instance = None
    __envVarService = None
    __scalerService = None
    __model = None
    __featureEngineering = None

    def __init__(self, featureEngineering: FeatureEngineering, envVarService: EnvVarService, scalerService: ScalerService):
        self.__featureEngineering = featureEngineering
        self.__envVarService = envVarService
        self.__scalerService = scalerService
        self.__scalerService.loadStats()
        #################################################
        key = LSTMPredictor.getModelName()
        #################################################
        
        self.__model = ModelFactory.getInstance(self.__envVarService, key)
        self.__model.loadModel()

    @staticmethod
    def getInstance(featureEngineering: FeatureEngineering, envVarService: EnvVarService, scalerService: ScalerService):
        if PredictorService.__instance == None:
            PredictorService.__instance = PredictorService(featureEngineering, envVarService, scalerService)
        return PredictorService.__instance

    def getHistoryData(self):
        dfFiltered = self.__featureEngineering.getDataRaw()
        df = self.__scalerService.transform(dfFiltered)
        return self.__scalerService.reverseTransformWithDate(df[['payments_sum']], ['payments_sum'])

    def transform2Column(self, df, colName = 'value'):
        d = TimeUtilities.mapDate(df.index.values[0])
        colNumbers = list(set([int(i.split('_')[2]) for i in df.columns]))
        colNumbers.sort()
        values = []
        for n in colNumbers:
            p = df['y_p_'+str(n)].values[0]
            p = np.round(p,0)
            v = df['y_v_'+str(n)].values[0]
            values.append(p*v)
        dataPredicted = pd.DataFrame(values, columns = [colName], index=[TimeUtilities.addMonths(d, i+1) for i,v in enumerate(values)])
        return dataPredicted

    def getPredictedData(self):
        dfFiltered = self.__featureEngineering.getDataRaw()

        xColumns, yColumns = self.__scalerService.getXYColumns()

        lastValue = dfFiltered.tail(1)[xColumns]
        lastValue = self.__scalerService.transform(lastValue, xColumns)
        dateNum = lastValue.index.values[0]

        X = lastValue
        yPred = self.__model.predict(X)
        yPred = pd.DataFrame(yPred, columns = yColumns, index = [dateNum])
        yPred = self.transform2Column(self.__scalerService.reverseTransformWithDate(yPred, yColumns), 'values')
        
        return yPred