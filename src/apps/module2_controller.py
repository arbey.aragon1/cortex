from plotly.subplots import make_subplots
import plotly.graph_objects as go
import dash_html_components as html
import dash_core_components as dcc
import dash_admin_components as dac
import plotly.express as px
import pandas as pd
import plotly.offline as py
import json
import dash
from dash.dependencies import Input, Output, State
import numpy as np
from datetime import datetime as dt
import datetime
import re
import os
from .data_warehouse_controller import DataWarehouseController, BalanceManagerController
from .predictor_service import PredictorService
from .shared.utility_service import UtilitiesService

class Module2Controller():
    __instance = None
    __balanceManagerController = None
    __predictorService = None


    def __init__(self, balanceManagerController: BalanceManagerController, predictorService: PredictorService):
        self.__balanceManagerController = balanceManagerController
        self.__predictorService = predictorService

    @staticmethod
    def getInstance(balanceManagerController: BalanceManagerController, predictorService: PredictorService):
        if Module2Controller.__instance == None:
            Module2Controller.__instance = Module2Controller(balanceManagerController, predictorService)
        return Module2Controller.__instance


    def __fig1(self, dfDataHist, dfDataPredict):
      
        fig = go.Figure()
        fig.add_trace(go.Scatter(x=list(dfDataHist.index), y=list(dfDataHist['payments_sum']), name='Recaudo', marker=dict(color='rgb(53,151,143)')))
        fig.add_trace(go.Scatter(x=list(dfDataPredict.index), y=list(dfDataPredict['values']), name='Predicción', marker=dict(color='rgb(128,205,193)')))
        fig.update_layout(
            margin={"r":0,"t":50,"l":0,"b":50},
            xaxis=dict(
                rangeselector=dict(
                    buttons=list([
                        dict(count=7,
                            label="1W",
                            step="day",
                            stepmode="backward"),
                        dict(count=1,
                            label="1M",
                            step="month",
                            stepmode="backward"),
                        dict(count=6,
                            label="6M",
                            step="month",
                            stepmode="backward"),
                        dict(count=12,
                            label="1Y",
                            step="month",
                            stepmode="backward"),
                        dict(step="all")
                    ])
                ),
                rangeslider=dict(
                    visible=True
                ),
                 type="date",
            showgrid=False,
            showline=False,
            showticklabels=True,
            zeroline=False,
            titlefont_size=4,
            ),
             yaxis=dict(
                showgrid=False,
                showline=False,
                showticklabels=True,
                zeroline=False,
                titlefont_size=12
             ),
        )
        return fig


    def __fig2(self, data):
        fig = go.Figure()
        fig.add_trace(go.Scatter(x=list(data.index), y=list(data['valor']), name='Recaudo', marker=dict(color='rgb(53,151,143)')))
        fig.update_layout(
            margin={"r":0,"t":50,"l":0,"b":50},
            xaxis=dict(
                rangeselector=dict(
                    buttons=list([
                        dict(count=7,
                            label="1W",
                            step="day",
                            stepmode="backward"),
                        dict(count=3,
                            label="3M",
                            step="month",
                            stepmode="backward"),
                        dict(count=6,
                            label="6M",
                            step="month",
                            stepmode="backward"),
                        dict(count=12,
                            label="1Y",
                            step="month",
                            stepmode="backward"),
                        dict(step="all")
                    ])
                ),
                rangeslider=dict(
                    visible=True
                ),
                 type="date",
            showgrid=False,
            showline=False,
            showticklabels=True,
            zeroline=False,
            titlefont_size=4,
            ),
             yaxis=dict(
                showgrid=False,
                showline=False,
                showticklabels=True,
                zeroline=False,
                titlefont_size=12
             ),
        )
        return fig


    def __fig3(self, data):
        fig = go.Figure()
        fig.add_trace(go.Scatter(x=list(data.index), y=list(data['valor']), name='Recaudo',  marker=dict(color='rgb(53,151,143)')))
        fig.update_layout(
            margin={"r":0,"t":50,"l":0,"b":50},
            xaxis=dict(
                rangeselector=dict(
                    buttons=list([
                        dict(count=7,
                            label="1W",
                            step="day",
                            stepmode="backward"),
                        dict(count=3,
                            label="3M",
                            step="month",
                            stepmode="backward"),
                        dict(count=6,
                            label="6M",
                            step="month",
                            stepmode="backward"),
                        dict(count=12,
                            label="1Y",
                            step="month",
                            stepmode="backward"),
                        dict(step="all")
                    ])
                ),
                rangeslider=dict(
                    visible=True
                ),
                type="date",
            showgrid=False,
            showline=False,
            showticklabels=True,
            zeroline=False,
            titlefont_size=4,
            ),
             yaxis=dict(
                showgrid=False,
                showline=False,
                showticklabels=True,
                zeroline=False,
                titlefont_size=12
             ),
        )
        return fig

    def __fig4(self):
        df = pd.read_csv("../data/sales_data_set.csv")
        df.head()
        df['Store'].value_counts()
        df1=df.loc[df['Store'] == 1]
        df2=df1.loc[df['Dept'] == 1]
        df2.head()

        df3 = df2[['Date','Weekly_Sales']]
        df3.rename(columns={'Date':'ds'}, inplace=True)
        df3.rename(columns={'Weekly_Sales':'y'}, inplace=True)
        df3.head()

        m = Prophet()
        m.fit(df3)

        future = m.make_future_dataframe(periods=52)
        future.tail()

        forecast = m.predict(future)
        forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail()

        fig = m.plot(forecast)

        fig = plot_plotly(m, forecast) 

        return fig


    def viewModule(self):
        historyData = self.__predictorService.getHistoryData()
        predictedData = self.__predictorService.getPredictedData()

        balaceNextMonth = UtilitiesService.getStrMoneyFromNumber(predictedData.iloc[0].values[0])
        balaceNext3Month = UtilitiesService.getStrMoneyFromNumber(predictedData.iloc[:3].sum().values[0])
        balaceNext6Month = UtilitiesService.getStrMoneyFromNumber(predictedData.iloc[:6].sum().values[0])

        balanceByMonth = self.__balanceManagerController.getBalanceByMonth()
        balanceByWeek = self.__balanceManagerController.getBalanceByWeek()
        viewModule = [ 
                   html.Div(
                    children=[
                        html.Div( 
                            className="row indicators",
                            children=[
                                html.Div(
                                    [
                                        html.P(balaceNextMonth, style={'color': '#000000', 'fontSize':30, 'font-family':'sans-serif', 'font-weight': 'bold'},),
                                        html.Div([html.P("Recaudo próximo mes")], className="subtitle"),
                                        html.P(className="twelve columns indicator_text"),
                                    ],
                                    className="four columns indicator pretty_container",
                                ),

                                html.Div(
                                    [
                                        html.P(balaceNext3Month, style={'color': '#000000', 'fontSize':30, 'font-family':'sans-serif', 'font-weight': 'bold'},),
                                        html.Div([html.P("Recaudo próximos 3 meses")], className="subtitle"),
                                        html.P(className="twelve columns indicator_text"),
                                    ],
                                    className="four columns indicator pretty_container",
                                ),
                                html.Div(
                                    [
                                        html.P(balaceNext6Month, style={'color': '#000000', 'fontSize':30, 'font-family':'sans-serif', 'font-weight': 'bold'}),
                                         html.Div([html.P("Recaudo próximos 6 meses")], className="subtitle"),
                                        html.P(className="twelve columns indicator_text"),
                                    ],
                                    className="four columns indicator pretty_container",
                                ),
                            
                            ],
                        ),
                        html.Div(
                            className="chart_div pretty_container",
                            children=[
                                html.Div([html.P("Predicción completa del flujo de caja")], className="subtitle"),
                                dcc.Graph(
                                    figure=self.__fig1(historyData, predictedData),
                                    style={"height": "90%", "width": "98%"},
                                    config=dict(displayModeBar=False),
                                ),
                            ],
                        ),
                        html.Div(
                            id="grid_cash_flow",
                            children=[
                                    html.Div(
                                    className="pretty_container",
                                    children=[
                                        html.Div([html.P("Ingresos semanales consolidados")], className="subtitle"),
                                        dcc.Graph(
                                            figure=self.__fig2(balanceByWeek),
                                            style={"height": "90%", "width": "98%"},
                                            config=dict(displayModeBar=False),
                                        ),
                                        
                                    ],
                                    ),
                                    html.Div(
                                        className="pretty_container",
                                        children=[
                                            html.Div([html.P("Ingresos mensuales consolidados")], className="subtitle"),
                                            dcc.Graph(
                                                figure=self.__fig3(balanceByMonth),
                                                style={"height": "90%", "width": "98%"},
                                                config=dict(displayModeBar=False),
                                            ),
                                            
                                        ],
                                    ),
                            ]
                        )
                    ],
                )
            ]
        
        return viewModule




