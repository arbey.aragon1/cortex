import dash_core_components as dcc
import dash_admin_components as dac
import dash_html_components as html
from dash.dependencies import Input, Output, State

add_file_tab = dac.TabItem(id='content_add_file',
                           children=html.Div([
                                html.Div(
                                   dac.Box(
                                   [
                                       dac.BoxHeader(
                                           collapsible=True,
                                           closable=False,
                                           title="Upload file from DRIVE",
                                           style={'textAlign': 'center'}
                                       ),
                                       dac.BoxBody(
                                           html.Div(
                                               [
                                                   dcc.ConfirmDialog(
                                                       id='confirm',
                                                       message='La petición de carga del archivo ha sido enviada!!',
                                                   ),
                                                   dcc.Input(
                                                       placeholder="URL de drive",
                                                       type="text",
                                                       value="",
                                                       id="url-drive"
                                                   ),
                                                   html.Div('Este campo es requerido',
                                                            id="url-drive-hidden",
                                                            hidden=True,
                                                            style={'color': 'red', 'fontSize': 14}),
                                                   html.Br(),
                                                   dcc.Input(
                                                       placeholder="Nombre del archivo(.xlsx)",
                                                       type="text",
                                                       value="",
                                                       id="file-name"
                                                   ),
                                                   html.Div('Este campo es requerido',
                                                            id="file-name-hidden",
                                                            hidden=True,
                                                            style={'color': 'red', 'fontSize': 14}),
                                                   html.Br(),
                                                   html.Button(
                                                       'Submit', id="submit-form")

                                               ], style={'width': '100%', 'margin': 0, 'textAlign': 'center'}
                                           )
                                       )
                                   ],
                                   style={'marginLeft': 0, 'marginRight': 0, 'marginTop': 0, 'marginBottom': 0,
                                                        'border': '', 'padding': '6px 0px 0px 8px',  "height" : "100%", 'width': "82vw" },
                                   gradient_color="dark",
                                   width=6
                                     )
                                ),
                               
                           ], style={"height": "90%", "width": "98%"}
                           
                    )
                )
