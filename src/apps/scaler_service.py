import pandas as pd
import os
import warnings
import numpy as np
import joblib
from .time_utilities import TimeUtilities
from .shared.env_var_service import EnvVarService

class ScalerService():
    __instance = None
    __envVarService = None
    __modelo = {
        'columns': None,
        'muScale': None,
        'stdScale': None,
        'yColumns': None,
        'xColumns': None,
    }
    
    def __init__(self, envVarService: EnvVarService):
        self.__envVarService = envVarService

    @staticmethod
    def getInstance(envVarService: EnvVarService):
        if ScalerService.__instance == None:
            ScalerService.__instance = ScalerService(envVarService)
        return ScalerService.__instance

    @staticmethod
    def getModelName():
        return 'ScalerService'

    def fit(self, df, columns = None):
        if(columns == None):
            self.__modelo['columns'] = df.columns
        else:
            self.__modelo['columns'] = columns
        self.__modelo['yColumns'] = [i for i in self.__modelo['columns'] if 'y_' in i]
        self.__modelo['xColumns'] = [i for i in self.__modelo['columns'] if 'y_' not in i]
        self.__modelo['muScale'] = df[self.__modelo['columns']].mean(axis=0)
        self.__modelo['stdScale'] = df[self.__modelo['columns']].std(axis=0)
    
    def saveStats(self, path = None):
        if(path == None):
            path = os.path.join(self.__envVarService.PATH_MODELS, ScalerService.getModelName()+".pkl")
        joblib.dump(self.__modelo, path) 

    def loadStats(self, path = None):
        if(path == None):
            path = os.path.join(self.__envVarService.PATH_MODELS, ScalerService.getModelName()+".pkl")
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=UserWarning)
            self.__modelo = joblib.load(path)

    def transform(self, df, columns = None):
        if(columns == None):
            columns = self.__modelo['columns'] 
        return (df[columns] - self.__modelo['muScale'][columns]) / self.__modelo['stdScale'][columns]

    def reverseTransform(self, df, columns = None):
        if(columns == None):
            columns = self.__modelo['columns'] 
        return  df[columns] * self.__modelo['stdScale'][columns] + self.__modelo['muScale'][columns]

    def reverseTransformWithDate(self, df, columns = None):
        if(columns == None):
            columns = self.__modelo['columns'] 
        df['index'] = df.index
        df.index = pd.to_datetime(df['index'].apply(TimeUtilities.mapDate))
        del df['index']
        df.sort_index() 
        return  df[columns] * self.__modelo['stdScale'][columns] + self.__modelo['muScale'][columns]

    def getXYColumns(self):
        return self.__modelo['xColumns'], self.__modelo['yColumns']

    def getStatistics(self):
        return self.__modelo