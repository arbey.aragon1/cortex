
import warnings
import numpy as np
import os

from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.models import Sequential
from keras.constraints import maxnorm

from .predictor_interface import PredictorInterface
from .shared.env_var_service import EnvVarService
class RNNPredictor(PredictorInterface):
    __instance = None
    __model = None
    __config = None
    
    def __init__(self, envVarService: EnvVarService):
        self.__envVarService = envVarService
      
    @staticmethod
    def getModelName():
        return 'RNNPredictor'

    @staticmethod
    def getInstance(envVarService: EnvVarService):
        if RNNPredictor.__instance == None:
            RNNPredictor.__instance = RNNPredictor(envVarService)
        return RNNPredictor.__instance
    
    @staticmethod
    def getDefaultParams():
        return {
            'inputShape': None,
            'outputShape': None,
            'epochs': 50,
            'units_1': 75,
            'units_2': 25 
        }

    def setParams(self, config):
        self.__config = config

    def createModel(self):
        self.__model = Sequential()
        self.__model.add(Dense(100, input_dim=self.__config['inputShape'], kernel_initializer='normal', activation='relu'))
        self.__model.add(Dense(self.__config['units_1'], kernel_initializer='normal', activation='relu', kernel_constraint=maxnorm(3)))
        self.__model.add(Dropout(0.2))
        self.__model.add(Dense(self.__config['units_2'], kernel_initializer='normal', activation='relu', kernel_constraint=maxnorm(3)))
        self.__model.add(Dropout(0.2))
        self.__model.add(Dense(self.__config['outputShape'], kernel_initializer='normal', activation='relu'))

    def fit(self, Xtrain, Ytrain, Xtest=None, Ytest=None):
        Xtrain = np.reshape(Xtrain.values, (Xtrain.shape[0], Xtrain.shape[1]))
        Ytrain = np.reshape(Ytrain.values, (Ytrain.shape[0], Ytrain.shape[1]))
        Xtest = np.reshape(Xtest.values, (Xtest.shape[0], Xtest.shape[1]))
        Ytest = np.reshape(Ytest.values, (Ytest.shape[0], Ytest.shape[1]))
        self.__model.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])
        history = self.__model.fit(Xtrain, Ytrain, epochs=self.__config['epochs'], batch_size=32, validation_data=(Xtest, Ytest))
        return history

    
    def predict(self, Xtest):
        y_predict = self.__model.predict(Xtest)
        return y_predict

    def saveModel(self, path = None):
        if(path == None):
            path = os.path.join(self.__envVarService.PATH_MODELS, "model"+RNNPredictor.getModelName()+".h5")
        self.__model.save(path)

    def loadModel(self, path = None):
        if(path == None):
            path = os.path.join(self.__envVarService.PATH_MODELS, "model"+RNNPredictor.getModelName()+".h5")
        self.__model = load_model(path)
