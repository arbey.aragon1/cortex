from plotly.subplots import make_subplots
import plotly.graph_objects as go
import dash_html_components as html
import dash_core_components as dcc
import dash_admin_components as dac
import plotly.express as px
import pandas as pd
import json
import dash
from dash.dependencies import Input, Output, State
import numpy as np
from datetime import datetime as dt
import datetime
import re
import os
from .data_warehouse_controller import DataWarehouseController, BalanceManagerController
from .shared.utility_service import UtilitiesService
from .shared.env_var_service import EnvVarService
from datetime import date
import json
from urllib.request import urlopen
app = dash.Dash(__name__)

class Module1Controller():
    __instance = None
    __balanceManagerController = None
    __envVarService = None

    color = {
    'AAA': "#eff0f4",
    'AA': "#74dbef",
    'A': "#0074e4",
    'B': "#264e86",
    }


    def __init__(self, balanceManagerController: BalanceManagerController, envVarService: EnvVarService):
        self.__balanceManagerController = balanceManagerController
        self.__envVarService = envVarService

    @staticmethod
    def getInstance(balanceManagerController: BalanceManagerController, envVarService: EnvVarService):
        if Module1Controller.__instance == None:
            Module1Controller.__instance = Module1Controller(balanceManagerController, envVarService)
        return Module1Controller.__instance


    def __fig1(self, labelsG, pieValue):
        fig = go.Figure(data=[go.Pie(labels=labelsG, values=pieValue, title='')])
        fig.update_traces(hoverinfo='value+percent', textinfo='value+percent', textposition='inside', insidetextorientation='radial',  textfont_size=16,
                            marker={"colors": [self.color[i] for i in labelsG]})
        fig.update_layout(uniformtext_minsize=16, uniformtext_mode='hide', showlegend=True, legend=dict(orientation="h", xanchor="center", yanchor="top", x=0.5, y=1.1), margin={"r":20,"t":20,"l":20,"b":10})
                                                                                                                                                                                               
        return fig


    def __fig2(self, x, y, clasificacion):
        fig = go.Figure([go.Bar(x=y, y=x, text=y,textposition='auto', width=[0.5] * len(x))])
        fig.update_traces(texttemplate='%{text:.2s}', textposition='auto', marker_line_color='rgb(0,0,0)', orientation='h', marker=dict(color=[self.color[i] for i in clasificacion]))
        fig.update_layout(
            margin={"r":0,"t":0,"l":0,"b":0},
            title='', 
            title_font=dict(family="Rockwell", size=12),  
            xaxis_tickfont_size=12,
            xaxis=dict(
            showgrid=False,
            showline=False,
            showticklabels=True,
            zeroline=False,
            domain=[0.15, 1],
            titlefont_size=4,
            
            ),
            yaxis=dict(
                showgrid=False,
                showline=False,
                showticklabels=True,
                zeroline=False,
                titlefont_size=12
             ),
        )
        return fig


    #def __fig3(self, df, hover_name = 'Nombre', hover_data = 'Cargos'):
    #    px.set_mapbox_access_token(self.__envVarService.MAPBOX_TOKEN)
    #    fig = px.scatter_mapbox(df, lat="lat", lon="lon",  color=hover_name, size=hover_data,
    #              color_discrete_sequence=[self.color[i] for i in df['qualification'].values], size_max=15, zoom=10)
    #    fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0}, showlegend=False)
    #    return fig


    def __fig4(self, meanTimeSinceStart):

        with urlopen('https://gist.githubusercontent.com/john-guerra/43c7656821069d00dcbc/raw/be6a6e239cd5b5b803c6e7c2ec405b793a9064dd/Colombia.geo.json') as response:
            counties = json.load(response)
        locs = ['VALLE DEL CAUCA']
        
        for loc in counties['features']:
            loc['id'] = loc['properties']['NOMBRE_DPT']

        def make_discrete_colorscale(vals, colors):
            colorscale = []
            
            zmin = vals[0]
            zmax = vals[-1]
            d = float(zmax-zmin)
            
            vals_normed = [(val-zmin)/d for val in vals]
            
            for i, val in enumerate(vals_normed[:-1]):
                colorscale.append([val, colors[i]])
                colorscale.append([val, colors[i+1]])
            
            colorscale.append([vals_normed[-1], colors[-1]])
            
            return colorscale
        
        vals = [0, 60, 120, 150, 250]
        colors = ['#eff0f4', '#eff0f4', '#74dbef', '#0074e4', '#264e86']
        colorscale = make_discrete_colorscale(vals, colors)

        fig = go.Figure(go.Choroplethmapbox(
            geojson=counties,
            locations=locs,
            zmin=min(vals),
            zmax=max(vals),
            z=[np.round(np.mean(meanTimeSinceStart),1)],
            colorscale=colorscale,
            marker_line_width=0.5,
            marker_line_color='rgba(0,0,0,0.1)',
            colorbar_title="Días",     
            #text=['label'], # hover text
        ))
        fig.update_layout(mapbox_style="carto-positron",
                        mapbox_zoom=4,
                        mapbox_center = {"lat": 3.43722, "lon": -76.52259},
                        margin={"r":0,"t":0,"l":0,"b":0})
        return fig

    def __conventions(self):

        colors = ['#eff0f4', '#74dbef', '#0074e4','#264e86']
        data = {'Range' : ['AAA:  menor a 60', 'AA:  entre 60 y 120', 'A: entre 120 y 150', 'B: mayor a 150'], 'Color' : colors}
        df = pd.DataFrame(data)

        fig = go.Figure([go.Table(
                    header=dict(
                        line_color='white', fill_color='white',
                        align='center', font=dict(color='black', size=12)
                    ),
                    cells=dict(
                        values=[df.Range],
                        line_color=[df.Color], fill_color=[df.Color],
                        align='center', font=dict(color='black', size=12)
                    )
                )
            ]
        )
        fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
        return fig

    def viewModule(self):
     
        totalBillCharges = UtilitiesService.getStrMoneyFromNumber(self.__balanceManagerController.getTotalBillCharges()) 
        totalSumPayment = UtilitiesService.getStrMoneyFromNumber(self.__balanceManagerController.getTotalSumPayment())
        totalSumUnpaid = UtilitiesService.getStrMoneyFromNumber(self.__balanceManagerController.getTotalSumUnpaidBills())
        

        dfUsersInfo = self.__balanceManagerController.getUserInfo().sort_values('meanTimeSinceStart', ascending = False)
        qualificationPercent =  dfUsersInfo.groupby(['qualification']).agg(sumBalance = ('lastBalance',lambda s: s.sum()))
       
     
        ### fig1() ###
        labelsG = qualificationPercent.index.values
        valuesG = qualificationPercent['sumBalance'].values
        pieValue =  UtilitiesService.moneyPieValue(valuesG)             

        ### fig2() ###
        clients = list(dfUsersInfo['fileName'].values)
        meanTimeSinceStart = list(dfUsersInfo['meanTimeSinceStart'].values)
        clasificacion = dfUsersInfo['qualification'].values

        ### fig3() ###
        dfMap = self.__balanceManagerController.getDataByMap()

        ### fig4() ###
        #mapClasification = list(np.round(np.mean(meanTimeSinceStart),1).values)
    
        viewModule = [ 

                html.Div(
                    id="cases_grid",
                    children=[                                   
                        html.Div(
                            id="cases_indicators",
                            className="row indicators",
                            children=[
                                html.Div(
                                [
                                    html.P(totalBillCharges, style={'color': '#000000', 'fontSize':30, 'font-family':'sans-serif', 'font-weight': 'bold'}),
                                    dcc.Markdown(''' **Facturado** '''),
                                    html.P(className="twelve columns indicator_text"),
                                ],
                                className="four columns indicator pretty_container",
                            ),
                            html.Div(
                                [
                                    
                                    html.P(totalSumPayment,  style={'color': '#000000', 'fontSize':30, 'font-family':'sans-serif', 'font-weight': 'bold'}),
                                    dcc.Markdown(''' **Recaudado** '''),
                                    html.P(className="twelve columns indicator_text"),
                                ],
                                className="four columns indicator pretty_container",
                            ),
                            html.Div(
                                [
                                    html.P(totalSumUnpaid,  style={'color': '#000000', 'fontSize':30, 'font-family':'sans-serif', 'font-weight': 'bold'}),
                                    dcc.Markdown(''' **Deuda** '''),
                                    html.P(className="twelve columns indicator_text"),
                                ],
                                className="four columns indicator pretty_container",
                            ),
                             html.Div(
                                [
                                    html.P(np.round(np.mean(meanTimeSinceStart),1),  style={'color': '#000000', 'fontSize':30, 'font-family':'sans-serif', 'font-weight': 'bold'}),
                                    dcc.Markdown(''' **Promedio Ponderado de días** '''),
                                    html.P(className="twelve columns indicator_text"),
                                ],
                                className="four columns indicator pretty_container",
                            ),
                            
                            ],
                        ),
                        

                        html.Div(    
                            id="cases_types_container",
                            className="pretty_container chart_div",
                            children=[
                                 html.Div(
                                    id="cases_indicators_2",
                                    className="row indicators",
                                    children=[
                                        html.Div(
                                        [
                                           
                                            html.Div(html.Img(src=app.get_asset_url('convenciones.png'), style={'height': '95%', 'width': '95%'})),
                                            dcc.Markdown(''' **Rango de clasificación por días** '''),
                                            html.P(className="twelve columns indicator_text"),
                                        ],
                                        className="four columns indicator pretty_container",
                                    ),
                                
                                    
                                    ],
                                ),
                                                        
                                html.Div([html.P("Distribución geográfica por clasificación")], className="subtitle"),
                                dcc.Graph(
                                    figure=self.__fig4(meanTimeSinceStart),
                                    id="cases_types",
                                    config=dict(displayModeBar=False),
                                    #style={"height": "100%", "width": "100%"},
                                ),
                            ],
                        ),
                        html.Div(
                            id="cases_reasons_container",
                            className="chart_div pretty_container",
                            children=[
                                html.Div([html.P("Promedio ponderado de días de cobranza")], className="subtitle"),
                                dcc.Graph(
                                    id="cases_reasons", 
                                    figure=self.__fig2(clients, meanTimeSinceStart, clasificacion),
                                    config=dict(displayModeBar=False),
                                    #style={"height": "75%", "width": "100%"},
                                                                
                                ),
                            ],
                        ),
                        html.Div(
                            id="cases_by_period_container",
                            className="pretty_container chart_div",
                            children=[
                                html.Div([html.P("Porcentaje y valor (Millones) de la cartera")], className="subtitle"),
                                dcc.Graph(
                                    id="cases_by_period",
                                    figure=self.__fig1(labelsG, pieValue),
                                    config=dict(displayModeBar=False),
                                    #style={"height": "75%", "width": "100%"},
                                ),
                            ],
                        ),
                    
                    ],
                ),
   
       
    ]
        

        return viewModule




