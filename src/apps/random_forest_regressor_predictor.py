import warnings
import numpy as np
import os
import joblib
from sklearn.ensemble import RandomForestRegressor

from .predictor_interface import PredictorInterface
from .shared.env_var_service import EnvVarService

class RandomForestRegressorPredictor(PredictorInterface):
    __instance = None
    __model = None
    __config = None
    
    def __init__(self, envVarService: EnvVarService):
        self.__envVarService = envVarService

    @staticmethod
    def getModelName():
        return 'RandomForestRegressorPredictor'

    @staticmethod
    def getInstance(envVarService: EnvVarService):
        if RandomForestRegressorPredictor.__instance == None:
            RandomForestRegressorPredictor.__instance = RandomForestRegressorPredictor(envVarService)
        return RandomForestRegressorPredictor.__instance
    
    @staticmethod
    def getDefaultParams():
        return {
            'inputShape': None,
            'outputShape': None,
            'epochs': 10,
            'n_estimators': 700,
            'max_depth': 1,
            'min_samples_split': 2
        }

    def setParams(self, config):
        self.__config = config

    def createModel(self):
        self.__modelo = RandomForestRegressor(n_estimators=self.__config['n_estimators'], \
                                                max_depth=self.__config['max_depth'], \
                                                min_samples_split=self.__config['min_samples_split'])

    def fit(self, Xtrain, Ytrain, Xtest=None, Ytest=None):
        Xtrain = np.reshape(Xtrain.values, (Xtrain.shape[0], Xtrain.shape[1]))
        Ytrain = np.reshape(Ytrain.values, (Ytrain.shape[0], Ytrain.shape[1]))
        self.__modelo.fit(Xtrain, Ytrain)

    def predict(self, Xtest):
        Xtest = np.reshape(Xtest.values, (Xtest.shape[0], Xtest.shape[1]))
        yPredict = self.__modelo.predict(Xtest)
        return yPredict

    def saveModel(self, path = None):
        if(path == None):
            path = os.path.join(self.__envVarService.PATH_MODELS, "model"+RandomForestRegressorPredictor.getModelName()+".pkl")
        joblib.dump(self.__modelo, path) 

    def loadModel(self, path = None):
        if(path == None):
            path = os.path.join(self.__envVarService.PATH_MODELS, "model"+RandomForestRegressorPredictor.getModelName()+".pkl")
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=UserWarning)
            self.__modelo = joblib.load(path)