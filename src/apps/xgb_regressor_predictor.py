
import joblib
import numpy as np
import os
import joblib
from xgboost import XGBRegressor
from xgboost.sklearn import XGBRegressor
from sklearn.multioutput import MultiOutputRegressor

from .predictor_interface import PredictorInterface
from .shared.env_var_service import EnvVarService

class XGBRegressorPredictor(PredictorInterface):
    __instance = None
    __model = None
    __config = None
    
    def __init__(self, envVarService: EnvVarService):
        self.__envVarService = envVarService

    @staticmethod
    def getModelName():
        return 'XGBRegressorPredictor'

    @staticmethod
    def getInstance(envVarService: EnvVarService):
        if XGBRegressorPredictor.__instance == None:
            XGBRegressorPredictor.__instance = XGBRegressorPredictor(envVarService)
        return XGBRegressorPredictor.__instance
    
    @staticmethod
    def getDefaultParams():
        return {
            'inputShape': None,
            'outputShape': None,
            'epochs': 10,
            'n_estimators': 650,
            'learning_rate': 0.045,
            'max_depth': 3,
            'min_child_weight': 3,
            'colsample_bytree': 0.8,
            'subsample': 0.5
        }

    def setParams(self, config):
        self.__config = config

    def createModel(self):
        self.__modelo = XGBRegressor(n_estimators=self.__config['n_estimators'], \
                                    learning_rate=self.__config['learning_rate'], \
                                    max_depth=self.__config['max_depth'], \
                                    min_child_weight=self.__config['min_child_weight'], \
                                    colsample_bytree=self.__config['colsample_bytree'], \
                                    subsample=self.__config['subsample'], \
                                    silent=1, \
                                    objective='reg:squarederror',  \
                                    verbose_eval=True)
        self.__modelo = MultiOutputRegressor(self.__modelo)

    def fit(self, Xtrain, Ytrain, Xtest=None, Ytest=None):
        Xtrain = np.reshape(Xtrain.values, (Xtrain.shape[0], Xtrain.shape[1]))
        Ytrain = np.reshape(Ytrain.values, (Ytrain.shape[0], Ytrain.shape[1]))
        self.__modelo.fit(Xtrain, Ytrain)

    def predict(self, Xtest):
        Xtest = np.reshape(Xtest.values, (Xtest.shape[0], Xtest.shape[1]))
        yPredict = self.__modelo.predict(Xtest)
        return yPredict

    def saveModel(self, path = None):
        if(path == None):
            path = os.path.join(self.__envVarService.PATH_MODELS, "model"+XGBRegressorPredictor.getModelName()+".pkl")
        joblib.dump(self.__modelo, path) 

    def loadModel(self, path = None):
        if(path == None):
            path = os.path.join(self.__envVarService.PATH_MODELS, "model"+XGBRegressorPredictor.getModelName()+".pkl")
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=UserWarning)
            self.__modelo = joblib.load(path)