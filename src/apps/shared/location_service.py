
import os
import sys
import requests
import re
import tempfile
import uuid
import googlemaps
import numpy as np
from .env_var_service import EnvVarService
from .db_service import DBService

class LocationService():
    __instance = None
    __envVarService = None
    __gmaps = None
    __dbService = None
    KEY_ADDRESSES_TABLE = 'addresses'

    def __init__(self, envVarService: EnvVarService, dbService: DBService):
        self.__envVarService = envVarService
        self.__dbService = dbService
        self.__connectClient()

    @staticmethod
    def getInstance(envVarService: EnvVarService, dbService: DBService):
        if LocationService.__instance == None:
            LocationService.__instance = LocationService(envVarService, dbService)
        return LocationService.__instance

    def __connectClient(self):
        self.__gmaps = googlemaps.Client(key=self.__envVarService.GOOGLEMAPS_TOKEN)

    def __getCoordinatesFromAPI(self, address):
        country = ', COLOMBIA'
        geocode_result = self.__gmaps.geocode(str(address) + ' ' + country)
        if len(geocode_result) > 0:
            return list(geocode_result[0]['geometry']['location'].values())
        else:
            return [np.NaN, np.NaN]

    def __getCoordinatesFromDB(self, address):
        try:
            return self.__dbService.getItem(LocationService.KEY_ADDRESSES_TABLE, address)['data']
        except:
            return [np.NaN, np.NaN]
        
    def __hasCoordinates(self, address):
        return self.__dbService.hasItem(LocationService.KEY_ADDRESSES_TABLE, address)

    def __setCoordinatesFromDB(self, address):
        print('Usa api para traer las coordenadas')
        coordinates = self.__getCoordinatesFromAPI(address)
        return self.__dbService.setItem(LocationService.KEY_ADDRESSES_TABLE, address, coordinates)

    def getCoordinatesFromAddress(self, address):
        if(not self.__hasCoordinates(address)):
            self.__setCoordinatesFromDB(address)
        return self.__getCoordinatesFromDB(address)