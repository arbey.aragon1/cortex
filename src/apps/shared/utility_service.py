import numpy as np
class UtilitiesService():
    @staticmethod
    def getStrMoneyFromNumber(number):
        number=int(round(number/1e6))
        number = "{:,.0f} M".format(number).replace(',','.')
        return number

    @staticmethod
    def moneyPieValue(list):
        round_list = [round(num/1e6) for num in list]
        money =[]
        for i in round_list:
            money.append(np.round(i,0))
        return money

    @staticmethod
    def moneyPieString(list):
        round_list = [round(num/1e6) for num in list]
        money =[]
        for i in round_list:
            money.append("{:,.0f} M".format(i))
        return money
         



