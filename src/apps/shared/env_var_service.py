import os
import os.path
class EnvVarService:
    __instance = None
    __fileENVPath = "../.env"
    DEFAULT_VALUES = {
        'ENVIRONMENT': 'STAGING',
        'HOST': 'localhost',
        'GOOGLEMAPS_TOKEN': None,
        'MAPBOX_TOKEN': None,
        'PATH_MODELS': None,
        'PATH_CREDENTIALS': None
    }

    def __init__(self):
        self.__loadVariables()

    @staticmethod
    def getInstance():
        if EnvVarService.__instance == None:
            EnvVarService.__instance = EnvVarService()
        return EnvVarService.__instance

    def __loadVariablesFromServer(self):
        for k in self.DEFAULT_VALUES.keys():
            if(k not in ['PATH_MODELS']):
                try:
                    setattr(self, k, os.environ[k])
                except:
                    print('EnvVarService: Error in load from server: ' + k)
            elif(k == 'PATH_MODELS'):
                try:
                    setattr(self, k, os.path.join(os.getcwd().split('src')[0], 'src', 'models'))
                except:
                    print('EnvVarService: Error in load from server: ' + k)
            
            

    def __loadENVFile(self):
        f = open(self.__fileENVPath, "r")
        variablesFromFile = [i.strip().split('=')
                             for i in f.read().split('\n') if len(i.strip())]
        for kv in variablesFromFile:
            try:
                setattr(self, kv[0], kv[1])
            except:
                print('EnvVarService: Error in load from file: ' + kv)

    def __loadVariables(self):
        if(os.path.exists(self.__fileENVPath)):
            self.__loadENVFile()
        else:
            self.__loadVariablesFromServer()
        
