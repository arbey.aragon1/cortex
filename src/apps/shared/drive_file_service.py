
import os
import sys
import requests
import re
import tempfile
import uuid

class DriveFileService():
    __instance = None
    def __init__(self):
        pass

    @staticmethod
    def getInstance():
        if DriveFileService.__instance == None:
            DriveFileService.__instance = DriveFileService()
        return DriveFileService.__instance

    def __downloadFileFromGoogleDrive(self, id, destination):
        URL = "https://docs.google.com/uc?export=download"

        session = requests.Session()

        response = session.get(URL, params={'id': id}, stream=True)
        token = self.__getConfirmToken(response)

        if token:
            params = {'id': id, 'confirm': token}
            response = session.get(URL, params=params, stream=True)

        self.__saveResponseContent(response, destination)

    def __getConfirmToken(self, response):
        for key, value in response.cookies.items():
            if key.startswith('download_warning'):
                return value

        return None

    def __saveResponseContent(self, response, destination):
        CHUNK_SIZE = 32768

        with open(destination, "wb") as f:
            for chunk in response.iter_content(CHUNK_SIZE):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)

    def __getKeyFromURL(self, url):
        key = ''
        for s in re.split('/|\.|:|\?|=', url):
            if(len(s) > len(key)):
                key = s
        return key

    def downloadFromDrive(self, key, url, fileName):
        file_id = self.__getKeyFromURL(url)
        path = os.path.join(tempfile.gettempdir(), key)
        access_rights = 0o755
        try:
            os.mkdir(path, access_rights)
        except OSError:
            print("Creation of the directory %s failed" % path)
        else:
            print("Successfully created the directory %s" % path)
        destination = os.path.join(path, fileName)
        self.__downloadFileFromGoogleDrive(file_id, destination)
        return destination

    def deleteFile(self, key):
        path = os.path.join(tempfile.gettempdir(), key)
        try:
            os.rmdir(path)
        except OSError as e:
            print("Error: %s : %s" % (path, e.strerror))
