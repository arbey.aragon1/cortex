
import sys
import os
import requests
import re
from tinydb import TinyDB, Query, where
import tempfile

class DBService():
    __instance = None
    __db = None

    def __init__(self):
        self.__createConnection()

    @staticmethod
    def getInstance():
        if DBService.__instance == None:
            DBService.__instance = DBService()
        return DBService.__instance

    def __createConnection(self):
        path = os.path.join(tempfile.gettempdir(), "db.json")
        #if os.path.exists(path):
        #    os.remove(path)
        #else:
        #    print('File does not exists')
        self.__db = TinyDB(path)

    def setItem(self, table, key, data):
        ta = self.__db.table(table)
        ta.insert({'key': key, 'data': data})

    def deleteItem(self, table, key):
        ta = self.__db.table(table)
        ta.remove(where('key') == key)

    def getItem(self, table, key):
        ta = self.__db.table(table)
        return ta.search(where('key') == key)[0]

    def hasItem(self, table, key):
        ta = self.__db.table(table)
        return len(ta.search(where('key') == key)) > 0

    def listItems(self, table):
        ta = self.__db.table(table)
        return ta.all()
