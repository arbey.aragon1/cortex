
import joblib
import warnings
import numpy as np
import os
import joblib
from sklearn.multioutput import MultiOutputRegressor
from sklearn.svm import SVR

from .predictor_interface import PredictorInterface
from .shared.env_var_service import EnvVarService

class SVRPredictor(PredictorInterface):
    __instance = None
    __model = None
    __config = None
    
    def __init__(self, envVarService: EnvVarService):
        self.__envVarService = envVarService

    @staticmethod
    def getModelName():
        return 'SVRPredictor'

    @staticmethod
    def getInstance(envVarService: EnvVarService):
        if SVRPredictor.__instance == None:
            SVRPredictor.__instance = SVRPredictor(envVarService)
        return SVRPredictor.__instance
    
    @staticmethod
    def getDefaultParams():
        return {
            'inputShape': None,
            'outputShape': None,
            'epochs': 10,
            'gamma': 1.0,
            'C':1.0
        }

    def setParams(self, config):
        self.__config = config

    def createModel(self):
        self.__modelo = SVR(kernel='rbf', C=self.__config['C'], gamma=self.__config['gamma'])
        self.__modelo = MultiOutputRegressor(self.__modelo)

    def fit(self, Xtrain, Ytrain, Xtest=None, Ytest=None):
        Xtrain = np.reshape(Xtrain.values, (Xtrain.shape[0], Xtrain.shape[1]))
        Ytrain = np.reshape(Ytrain.values, (Ytrain.shape[0], Ytrain.shape[1]))
        self.__modelo.fit(Xtrain, Ytrain)

    def predict(self, Xtest):
        Xtest = np.reshape(Xtest.values, (Xtest.shape[0], Xtest.shape[1]))
        yPredict = self.__modelo.predict(Xtest)
        return yPredict

    def saveModel(self, path = None):
        if(path == None):
            path = os.path.join(self.__envVarService.PATH_MODELS, "model"+SVRPredictor.getModelName()+".pkl")
        joblib.dump(self.__modelo, path) 

    def loadModel(self, path = None):
        if(path == None):
            path = os.path.join(self.__envVarService.PATH_MODELS, "model"+SVRPredictor.getModelName()+".pkl")
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=UserWarning)
            self.__modelo = joblib.load(path)