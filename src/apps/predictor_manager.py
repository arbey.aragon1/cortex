class PredictorManager():
    __df = None
    __models = {}
    def __init__(self, df):
        self.__df = df
        self.__yColumns = [i for i in df.columns if 'y_' in i]
        self.__xColumns = [i for i in df.columns if 'y_' not in i]
        self.XYData = {}

        self.__models[RNNPredictor.getModelName()] = {'epochs': 10}
        self.__models[LSTMPredictor.getModelName()] = {'epochs': 10}
        self.__models[XGBRegressorPredictor.getModelName()] = {'epochs': 10}
        self.__models[LinearRegressionPredictor.getModelName()] = {'epochs': 10}
        self.__models[RandomForestRegressorPredictor.getModelName()] = {'epochs': 10}
        self.trainModel()
            
    def trainModel(self):
        
        quartersRaw = list(self.__df.index.unique())
        quartersRaw.sort()
        quarters = [i for i in quartersRaw if 201700 <= i]
        f = lambda A, n=9: [A[i:i+n] for i in range(0, len(A), n)]
        for ar in f(quarters)[:]:
            q = ar[0]
            train = df[df.index < q]
            test = df[df.index.isin(ar)]
            Xtrain, Ytrain = train[self.__xColumns], train[self.__yColumns]
            Xtest, Ytest = test[self.__xColumns], test[self.__yColumns]
            
            self.XYData[str(q)+'_Xtrain'] = Xtrain.copy()
            self.XYData[str(q)+'_Ytrain'] = Ytrain.copy()
            self.XYData[str(q)+'_Ytest'] = Ytest.copy()

            for k in list(self.__models.keys())[:]:
                print(q, k)
                self.__models[k]['model'] = ModelFactory.getInstance(k, len(self.__xColumns), len(self.__yColumns), self.__models[k]['epochs'])
                history = self.__models[k]['model'].fit(Xtrain, Ytrain, Xtest, Ytest)
                Ytrainpred = self.__models[k]['model'].predict(Xtrain)
                Ytestpred = self.__models[k]['model'].predict(Xtest)
                self.__models[k]['metrics_'+str(q)] = ErrorMetrics.generateMetrics(Ytest, Ytestpred)
                self.__models[k]['history_'+str(q)] = history
                self.__models[k]['model'].saveModel()

                for i,v in enumerate(self.__yColumns): 
                    self.XYData[str(q)+'_Ytrain'][v+'_Ytrainpred_'+k] = Ytrainpred[:,i]
                    self.XYData[str(q)+'_Ytest'][v+'_Ytestpred_'+k] = Ytestpred[:,i]

    def getMetrics(self):
        return self.__models

    def getXYdata(self):
        return self.XYData

    def getModel(self, k):
        return self.__models[k]['model']

    def getXYColumns(self):
        return self.__xColumns, self.__yColumns