from datetime import date
import uuid
import tempfile
import re
import requests
import os
import pandas as pd
import numpy as np
import calendar

from .firestore_service import FirestoreService
from .shared.db_service import DBService
from .shared.location_service import LocationService


class DataWarehouseController():
    __instance = None
    __fileService = None
    __dbService = None
    __locationService = None
    

    __filesData = None
    __dataTable = None
    __dataProfiles = None

    __indexMap = None

    KEY_FILES_TABLE = 'files'

    def __init__(self, fileService: FirestoreService, dbService: DBService, locationService: LocationService):
        self.__fileService = fileService
        self.__dbService = dbService
        self.__locationService = locationService
        self.__loadData()

    @staticmethod
    def getInstance(fileService: FirestoreService, dbService: DBService, locationService: LocationService):
        if DataWarehouseController.__instance == None:
            DataWarehouseController.__instance = DataWarehouseController(fileService, dbService, locationService)
        return DataWarehouseController.__instance
    
    def __loadFile(self, index, path):
        meta = pd.read_excel(path,
                      sheet_name='kortex',
                      nrows= 4,  
                      usecols = 'A').to_dict()
        df = pd.read_excel(path,
                      sheet_name='kortex',
                      skiprows = 5,  
                      usecols = 'A:H')
        
        df.columns = df.columns.str.strip()
        df = df.dropna(how='all')

        columnNames = ['Tipo Docto', 'Numero', 'Fecha', 'Promedio', 'Cargos', 'Abonos', 'Nuevo Saldo', 'Enviado a']
        columnNames = [i.strip().replace(' ','_').lower() for i in columnNames]
        df.columns = columnNames

        balance = dict(df.loc[0])['nuevo_saldo']
        df = df.drop(df.index[0])

        meta['saldo'] = 0
        if ((abs(balance) > 0) and(True)):
            meta['saldo'] = abs(balance)
        
        return df, meta

    def __loadFilesData(self):
        for key in self.__filesData.keys():
            self.__filesData[key]['data'], self.__filesData[key]['meta'] = self.__loadFile(key, self.__filesData[key]["path"])

    def __loadData(self):
        pathList = self.__dbService.listItems(DataWarehouseController.KEY_FILES_TABLE)
        print(pathList)
        pathList = [[i['data']['fileName'].split('.')[0], i['data']['path']] for i in pathList]

        self.__filesData = {str(i): {'fileName': np[0], 'path': np[1]} for i,np in enumerate(pathList) if '.xlsx' in np[1]}
        self.__loadFilesData()

        def dataAddId(df,id):
            df['id'] = id
            return df

        self.__dataTable = pd.concat([dataAddId(self.__filesData[k]['data'],k) for k in self.__filesData])
        self.__dataTable['fecha'] = pd.to_datetime(self.__dataTable['fecha'])  
        self.__dataTable['fileName'] = self.__dataTable.apply(lambda s: self.__filesData[str(s.id)]['fileName'], axis=1)
        
        ###############################################################
        self.__dataTable['new_index'] = 1
        self.__dataTable['new_index'] = self.__dataTable['new_index'].cumsum()
        self.__indexMap = self.__dataTable[['new_index', 'numero']]
        self.__dataTable['numero'] = self.__dataTable['new_index']
        del self.__dataTable['new_index']
        ###############################################################

        self.__loadCoordinates()

    def __getProfiles(self, indexUser):
        print(indexUser)
        dfUser = self.__dataTable[self.__dataTable['id'] == indexUser].sort_values(by = 'fecha').reset_index()
        del dfUser['index']

        dfData = dfUser[['fecha', 'cargos', 'abonos', 'tipo_docto', 'numero']]
        dfData['saldo'] = 0
        dfData['ab_updates'] = dfData['abonos']
        balanceInicial = self.__filesData[indexUser]['meta']['saldo']

        #print('Balance inicial: ',balanceInicial)

        indicesCargos = dfUser[dfUser['cargos'] != 0].index
        indicesAbonos = dfUser[dfUser['abonos'] != 0].index

        dicPerfiles = {}

        seguimientoAbonos = [dfData['abonos']]
        ## Iteramos sobre los cargos
        for indiceCargo in indicesCargos[:]:
            #print()
            #print('*-*-*-*-*-*-**-*-*-*-*-*-**-*-*-*-*-*-**-*-*-*-*-*-*')
            ## Traemos el identificador de la factura
            idInvoice = dfUser.loc[indiceCargo,'numero']
            #print('id factura: ',idInvoice)

            ## Traemos el valor del cargo
            valorCargo = dfData.loc[indiceCargo, 'cargos']
            fechaCargo = dfData.loc[indiceCargo, 'fecha']
            tipoDocto = dfData.loc[indiceCargo, 'tipo_docto']
            #print('valor cargo: ',valorCargo)

            ## creamos una lista vacia para el perfil de pagos
            dicPerfiles[idInvoice] = [{
                'fecha': fechaCargo, 
                'valor':valorCargo, 
                'tipo':'cargo', 
                'tipo_docto': tipoDocto,
                'numero_factura': idInvoice}]

            ## Contabilizador de pago
            pago = 0
            pagoFinalizado = False


            ## Adiciona saldo previo
            if(balanceInicial > 0):
                deltaBalance = 0
                if (balanceInicial <= valorCargo):
                    if(balanceInicial == valorCargo):
                        pagoFinalizado = True
                    #print('+condicion 1: ', balanceInicial)
                    deltaBalance = balanceInicial

                elif (balanceInicial > valorCargo):
                    #print('+condicion 2: ', valorCargo)
                    pagoFinalizado = True
                    deltaBalance = valorCargo
                    
                pago += deltaBalance
                balanceInicial -= deltaBalance
                dicPerfiles[idInvoice].append({
                    'fecha': fechaCargo, 
                    'valor': deltaBalance, 
                    'tipo': 'balanceInicial',
                    'tipo_docto': tipoDocto,
                    'numero_factura': idInvoice
                    })

            #print('#########Iteracion de bonos')
            ## Iteramos sobre los abonos
            for indiceAbono in indicesAbonos:
                valorAbono = dfData.loc[indiceAbono, 'ab_updates']
                numeroAbono = dfData.loc[indiceAbono, 'numero']
                fechaAbono = dfData.loc[indiceAbono, 'fecha']
                tipoDoctoAbono = dfData.loc[indiceAbono, 'tipo_docto']
                
                if (not pagoFinalizado):
                    if (valorAbono != 0):
                        if(valorAbono < 0):
                            print("\x1b[31m\"Error\"\x1b[0m",valorAbono)
                        valorAPagar = 0
                        if ((pago + valorAbono) <= valorCargo):
                            if((pago + valorAbono) == valorCargo):
                                pagoFinalizado = True
                            valorAPagar = valorAbono
                            #print('condicion 1: ', valorAPagar)

                        elif ((pago + valorAbono) > valorCargo):
                            pagoFinalizado = True
                            valorAPagar = valorCargo - pago
                            #print('condicion 2: ', valorAPagar)

                        pago += valorAPagar
                        #print('--',indiceAbono, dfData.loc[indiceAbono, 'ab_updates'])
                        dfData.loc[indiceAbono, 'ab_updates'] -= valorAPagar
                        #print('++',indiceAbono, dfData.loc[indiceAbono, 'ab_updates'])
                        dicPerfiles[idInvoice].append({
                            'fecha': fechaAbono, 
                            'valor':valorAPagar, 
                            'tipo':'abono',
                            'tipo_docto': tipoDoctoAbono,
                            'numero_factura': numeroAbono
                            })
                else: 
                    break
            #print(dicPerfiles[idInvoice])
            seguimientoAbonos.append(dfData['ab_updates'].copy())

        dfList = []
        for k in list(dicPerfiles.keys())[:]:
            dfTemp = pd.DataFrame(dicPerfiles[k])
            dfTemp['diff'] = (dfTemp['fecha'] - dfTemp['fecha'].shift(1)).apply(lambda s: s.days)
            dfTemp['timeSinceStart'] = (dfTemp['fecha'] - dfTemp['fecha'].iloc[0]).apply(lambda s: s.days)
            dfTemp['numero_cargo'] =  str(int(k))
            dfTemp['paymentFinished'] = (dfTemp['valor'].iloc[:1].sum() - dfTemp['valor'].iloc[1:].sum()) == 0
            dfTemp['id'] =  indexUser
            dfList.append(dfTemp)
            #print(dfTemp)

        perfiles = pd.concat(dfList, axis=0)
        #perfiles = dfList
        return perfiles

    def __loadProfiles(self):
        self.__dataProfiles = pd.concat([self.__getProfiles(k) for k in self.__filesData.keys()], axis=0)
        
    def __loadCoordinates(self):
        addresses = pd.DataFrame(self.__dataTable['enviado_a'].unique())
        addresses[1] = addresses[0].apply(lambda s: self.__locationService.getCoordinatesFromAddress(s))
        addresses[2] = addresses[1].apply(lambda s: s[0])
        addresses[3] = addresses[1].apply(lambda s: s[1])
        del addresses[1]
        addresses.columns = ['enviado_a', 'lat', 'lon']
        self.__dataTable = pd.merge(self.__dataTable,
              addresses,
              on='enviado_a')
        self.__loadProfiles()


    def getDataTable(self):
        return self.__dataTable

    def getFilesData(self):
        return self.__filesData

    def getDataProfiles(self):
        return self.__dataProfiles


class BalanceManagerController():
    __instance = None
    __dataWarehouseController = None
    
    def __init__(self, dataWarehouseController: DataWarehouseController):
        self.__dataWarehouseController = dataWarehouseController

    @staticmethod
    def getInstance(dataWarehouseController: DataWarehouseController):
        if BalanceManagerController.__instance == None:
            BalanceManagerController.__instance = BalanceManagerController(dataWarehouseController)
        return BalanceManagerController.__instance

    def getTotalAmountPortfolio(self):
        profiles = self.__dataWarehouseController.getDataProfiles()
        totalSumUnpaidBills = self.getTotalSumUnpaidBills()
        payBillCharges = profiles[(~profiles['paymentFinished']) & (profiles['tipo'] != 'cargo')]['valor'].sum()
        return totalSumUnpaidBills - payBillCharges

    def getCollectionDaysPerCustomer(self):
        limites = {
            'AAA':{'min':-np.Inf,'max':60},
            'AA':{'min':60,'max':120},
            'A':{'min':120,'max':150},
            'B':{'min':150,'max':np.Inf}
        }
        def getQualification(s):
            value = s.mean()
            qualification = ''
            for k in limites.keys():
                if ((limites[k]['min']<=value) & (limites[k]['max']>value)):
                    qualification = k
                    break
            return qualification
        profiles = self.__dataWarehouseController.getDataProfiles()
        dataFiles = self.__dataWarehouseController.getFilesData()
        return profiles[profiles['paymentFinished']]\
          .groupby(['numero_cargo'])\
          .agg(maxTimeSinceStart = ('timeSinceStart', lambda s: s.max()), \
              id = ('id', lambda s: s.iloc[-1]))\
          .groupby(['id'])\
          .agg(meanTimeSinceStart = ('maxTimeSinceStart', lambda s: s.mean()), \
               qualification = ('maxTimeSinceStart', getQualification), \
              fileName = ('id', lambda s: dataFiles[str(s.iloc[-1])]['fileName']))
          
    def getLastBalance(self):
        dataFiles = self.__dataWarehouseController.getFilesData()
        data = self.__dataWarehouseController.getDataTable()

        usersBalance = data.sort_values('fecha')\
          .groupby(['id'])\
          .agg(lastBalance = ('nuevo_saldo', lambda s: s.iloc[-1]), \
              fileName = ('id', lambda s: dataFiles[str(s.iloc[-1])]['fileName']))
        return usersBalance

    def getUserInfo(self):
        dfCollectionDaysPerCustomer = self.getCollectionDaysPerCustomer()
        dfLastBalance = self.getLastBalance()
        return pd.merge(dfCollectionDaysPerCustomer,
                 dfLastBalance)

    def getBalanceByMonth(self, year, idUser):
        data = self.__dataWarehouseController.getDataTable()
        dataTemp = data[data['id']==str(idUser)]
        dataTemp['year'] = dataTemp['fecha'].dt.year
        dataTemp['month'] = dataTemp['fecha'].dt.month

        listDataMonth = []
        for i in range(1,13):
            date = str(year)+'-'+str(i)+'-'+str(calendar.monthrange(year,i)[1])
            listDataMonth.append(dataTemp[dataTemp['fecha']<=date].sort_values('fecha')\
                .groupby(['id'])\
                .agg(lastBalance = ('nuevo_saldo', lambda s: s.iloc[-1])))
            
        dfD = pd.concat(listDataMonth, axis=1).fillna(0)
        dfD.columns = list(range(1,13))
        return dfD.values[0]

    def getDataByMap(self):#date
        dfUsersInfo = self.getUserInfo()
        data = self.__dataWarehouseController.getDataTable()
        data = pd.merge(data,
                      dfUsersInfo[['qualification', 'fileName']],
                      on='fileName')
        dat = data \
          .groupby(['lon','lat','id']) \
          .agg({
                'fecha': lambda x: x.iloc[-1], 
                'cargos': lambda x: x.sum(),
                'lon': lambda x: x.iloc[-1],
                'lat': lambda x: x.iloc[-1],
                'fileName': lambda x: x.iloc[-1],
                'qualification': lambda x: x.iloc[-1],
                }) \
          .rename(columns={'fecha': 'Fecha', 'cargos': 'Cargos', 'fileName': 'Nombre'})
        dat = dat.dropna()
        return dat

    def getBalanceByMonth(self):
        profiles = self.__dataWarehouseController.getDataProfiles()
        return profiles[profiles['tipo'] == 'abono'].resample('M', on='fecha')[['valor']].mean().fillna(0)

    def getBalanceByWeek(self):
        profiles = self.__dataWarehouseController.getDataProfiles()
        return profiles[profiles['tipo'] == 'abono'].resample('W', on='fecha')[['valor']].mean().fillna(0)

    def getTotalBillCharges(self):
        return self.__dataWarehouseController.getDataTable()['cargos'].sum()

    def getTotalSumPayment(self):
        profiles = self.__dataWarehouseController.getDataProfiles()
        return self.__dataWarehouseController.getDataTable()['abonos'].sum() + profiles[profiles['tipo'] == 'balanceInicial']['valor'].sum()

    def getTotalSumUnpaidBills(self):
        profiles = self.__dataWarehouseController.getDataProfiles()
        paymentFinished = profiles[~profiles['paymentFinished']]
        value = paymentFinished[paymentFinished['tipo'] == 'cargo']['valor'].sum() - paymentFinished[paymentFinished['tipo'] == 'abono']['valor'].sum()
        return value
