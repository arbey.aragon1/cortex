import pandas as pd
import calendar
import datetime

class TimeUtilities():
    @staticmethod
    def mapDate(s):
        dat = str(s)
        return pd.Timestamp(dat[:4]+'-'+dat[4:])

    @staticmethod
    def addMonths(sourcedate, months):
        month = sourcedate.month - 1 + months
        year = sourcedate.year + month // 12
        month = month % 12 + 1
        day = min(sourcedate.day, calendar.monthrange(year,month)[1])
        return pd.Timestamp(datetime.date(year, month, day))
