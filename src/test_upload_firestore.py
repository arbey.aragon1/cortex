import requests
import os

from apps.firestore_service import FirestoreService
from apps.shared.db_service import DBService
from apps.shared.env_var_service import EnvVarService

import firebase_admin
from firebase_admin import credentials, firestore, storage

envVarService = EnvVarService.getInstance()
fileService = FirestoreService.getInstance(envVarService)

if __name__ == "__main__":
    print('start')
    cred = credentials.Certificate('C:/Users/Oscar/Documents/cortex/serviceAccountKey.json')

    firebase_admin.initialize_app(cred, {
        'storageBucket': 'halogen-brace-266119.appspot.com'
    })

    db = firestore.client()
    bucket = storage.bucket()

    
    blob = bucket.blob('hello.txt')
    outfile='C:/Users/Oscar/Documents/cortex/README.md'
    with open(outfile, 'rb') as my_file:
        blob.upload_from_file(my_file)

    localFolder = 'C:/Users/Oscar/Documents/cortex/'
    blob = bucket.blob('hello.txt')
    fileName = blob.name.split('/')[-1]
    blob.download_to_filename(localFolder + fileName)
    print(f'{fileName} downloaded from bucket.')


    blob = bucket.blob('hello.txt')
    blob.delete()

