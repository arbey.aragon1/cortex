import os
import dash
from dash.dependencies import Input, Output, State
import dash_bootstrap_components as dbc
import dash_html_components as html
import dash_core_components as dcc
import dash_admin_components as dac
from dash.exceptions import PreventUpdate
from apps.home import home_tab
from apps.module1 import module1_tab
from apps.module2 import module2_tab
from apps.module3 import module3_tab
from apps.invoice import invoice_tab
from apps.warning import warning_tab
from apps.add_file import add_file_tab
from apps.file_list import file_list_tab
from datetime import datetime as dt
import re
from apps.firestore_service import FirestoreService
from apps.shared.db_service import DBService
from apps.file_manager_controller import FileManagerController
from apps.module1_controller import Module1Controller
from apps.module2_controller import Module2Controller
from apps.data_warehouse_controller import DataWarehouseController

from apps.main_module import MainModule

environment = 'STAGING'
try:
    environment = os.environ['ENVIRONMENT']
except:
    pass

host = 'localhost'
if(environment == 'PRODUCTION'):
    host = '0.0.0.0'


mainModule = MainModule.getInstance()
fileService = mainModule.FileService()
dbService = mainModule.DBService()
fileManagerController = mainModule.FileManagerController()

dataWarehouseController = mainModule.DataWarehouseController()

module1Controller = mainModule.Module1Controller()

module2Controller = mainModule.Module2Controller()
# =============================================================================
# Dash App and Flask Server
# =============================================================================
app = dash.Dash(__name__)
app.title = 'CORTEX'
server = app.server

# =============================================================================
# Dash Admin Components

# ============================================================================

# navbar
text = dcc.Markdown('''
    #### CORTEX
    
''')

navbar = dac.Navbar(color="dark",  children=text)



# Sidebar
sidebar = dac.Sidebar(
    dac.SidebarMenu(
        [
            dac.SidebarHeader(children="Home"),
            dac.SidebarMenuItem(id='tab_home', label='Home', icon='home'),

            dac.SidebarHeader(children="Modules"),
            dac.SidebarMenuItem(
                id='tab_module1', label='Display', icon='chart-line'),
            dac.SidebarMenuItem(
                id='tab_module2', label='Estimated Cash Flow', icon='id-card'),

        ]
    ),
    title='empathea advisors',
    skin="light",
    color="dark",
    brand_color="dark",
    src=app.get_asset_url('logo.png'),
    elevation=5,
    opacity=1.0
)

# Body
body = dac.Body(
    dac.TabItems([
        home_tab,
        module1_tab,
        module2_tab,
        invoice_tab,
        add_file_tab,
        file_list_tab,
    ])
)
# controlbar - calendar
app.layout =html.Div([
                html.Div([
                    dcc.Markdown(''' **Calendario** ''')
                ]),
                html.Div([
                    dcc.DatePickerSingle(
                        id='my-date-picker-single1',
                        min_date_allowed=dt(2015, 7, 15),
                        max_date_allowed=dt(2020, 6, 23),
                        date=str(dt(2020, 6, 23, 23, 59, 59))
                    ),html.Div(id='output-container-date-picker-single1')
                ]),
                html.Div([
                        dcc.DatePickerSingle(
                            id='my-date-picker-single2',
                            min_date_allowed=dt(2015, 7, 15),
                            max_date_allowed=dt(2020, 6, 23),
                            date=str(dt(2020, 6, 23, 23, 59, 59))
                    ),html.Div(id='output-container-date-picker-single2')
                ])
            ], className ='one columns'

)

controlbar = dac.Controlbar(
   app.layout
)

# Footer
footer = dac.Footer(
    html.A("",
           target="_blank",
           ),
    right_text="2020"
)

# =============================================================================
# App Layout
# =============================================================================
app.layout = dac.Page([navbar, sidebar, body, footer])

# =============================================================================
# Callbacks
# =============================================================================


def activate(input_id, n_home, n_module1, n_module2):

    if input_id == 'tab_home' and n_home:
        return True, False, False
    if input_id == 'tab_module1' and n_module1:
        return False, True, False
    elif input_id == 'tab_module2' and n_module2:
        return False, False, True
    else:
        return False, True, False


@app.callback([Output('content_home', 'active'),
               Output('content_module1', 'active'),
               Output('content_module2', 'active')],
              [Input('tab_home', 'n_clicks'),
               Input('tab_module1', 'n_clicks'),
               Input('tab_module2', 'n_clicks')]
              )
def display_tab(*args):
    ctx = dash.callback_context
    if not ctx.triggered:
        raise PreventUpdate
    else:
        input_id = ctx.triggered[0]['prop_id'].split('.')[0]
    return activate(input_id, *args)


@app.callback([Output('tab_home', 'active'),
               Output('tab_module1', 'active'),
               Output('tab_module2', 'active')],
              [Input('tab_home', 'n_clicks'),
               Input('tab_module1', 'n_clicks'),
               Input('tab_module2', 'n_clicks')]
              )
def activate_tab(*args):
    ctx = dash.callback_context
    if not ctx.triggered:
        raise PreventUpdate
    else:
        input_id = ctx.triggered[0]['prop_id'].split('.')[0]
    return activate(input_id, *args)


# Output('file-name', 'value'),


# =============================================================================
# File list
# =============================================================================
@app.callback([
    Output('url-drive', 'value'),
    Output('file-name', 'value'),
    Output('url-drive-hidden', 'hidden'),
    Output('file-name-hidden', 'hidden'),
    Output('confirm', 'displayed')
],
    [Input('submit-form', 'n_clicks')],
    [State('url-drive', 'value'), State('file-name', 'value')])
def load_files(n_clicks, url, fileName):
    if((url == '') and (fileName == '')):
        return '', '', False, False

    if((url == '') or (fileName == '')):
        return url, fileName, False, False, False

    print(url)
    print(fileName)
    ######fileManagerController.loadAndSaveFile(url, fileName)
    print(fileManagerController.getFileListData())
    return '', '', True, True, True

# =============================================================================
# Calendar
# =============================================================================
@app.callback(
    Output('output-container-date-picker-single1', 'children'),
    [Input('my-date-picker-single1', 'date')])
def update_output1(date):
    string_prefix = 'Fecha Inicial: '
    if date is not None:
        date = dt.strptime(re.split('T| ', date)[0], '%Y-%m-%d')
        date_string = date.strftime('%B %d, %Y')
        return string_prefix + date_string

@app.callback(
    Output('output-container-date-picker-single2', 'children'),
    [Input('my-date-picker-single2', 'date')])
def update_output2(date):
    string_prefix = 'Fecha Final: '
    if date is not None:
        date = dt.strptime(re.split('T| ', date)[0], '%Y-%m-%d')
        date_string = date.strftime('%B %d, %Y')
        return string_prefix + date_string

# =============================================================================
# Module1
# =============================================================================

@app.callback(
    Output('content_module1', 'children'),
    [Input('my-date-picker-single1', 'date'),
     Input('my-date-picker-single2', 'date'),
     Input('select-year', 'value'),
     Input('select-user', 'value'),
     ])
def update_module1(date1, date2, selectYear, selectUser):
    dataFilter = {
        'startDate': date1,
        'endDate': date2,
        'selectYear': selectYear,
        'selectUser': selectUser,
        
    }
    return module1Controller.viewModule(dataFilter)


# =============================================================================
# Run app
# =============================================================================
if __name__ == '__main__':
    app.run_server(debug=False, host=host, port='5000')
