import requests
import os
import uuid

from apps.firestore_service import FirestoreService
from apps.shared.db_service import DBService
from apps.shared.env_var_service import EnvVarService

import firebase_admin
from firebase_admin import credentials, firestore, storage

envVarService = EnvVarService.getInstance()
fileService = FirestoreService.getInstance(envVarService)

if __name__ == "__main__":
    print('start')
    localFolder = 'C:/Users/Oscar/Documents/cortex/'
    fileName = 'README.md' 
    key = 'test'

    files = [
        ['BEEMMODA.xlsx','239d9f5c-f97e-11ea-82c9-181deaf44bd2'],
        ['CRISTIANDRADE.xlsx','2ad5cb1a-f97e-11ea-9ed2-181deaf44bd2'],
        ['DIANASANCHEZ.xlsx','2b364198-f97e-11ea-8783-181deaf44bd2'],
        ['EL UNIVERSO.xlsx','2ba21db4-f97e-11ea-bc11-181deaf44bd2'],
        ['FAMOKA11.xlsx','2c05dfee-f97e-11ea-9164-181deaf44bd2'],
        ['GOTAGUA.xlsx','2c7fa4b0-f97e-11ea-b00f-181deaf44bd2'],
        ['ISAZAJOHAN.xlsx','2cf0fb36-f97e-11ea-9a30-181deaf44bd2'],
        ['LABODEGA.xlsx','2d5cce50-f97e-11ea-9c7c-181deaf44bd2'],
        ['SOCIEDAD.xlsx','2dcc92d2-f97e-11ea-a7bd-181deaf44bd2'],
        ['UNITED.xlsx','2e419f76-f97e-11ea-915f-181deaf44bd2'],
        ['WILLIAMROJAS.xlsx','2e8f6f28-f97e-11ea-8ed7-181deaf44bd2'],
        ['WILSONARISTIZABAL.xlsx','2eed8c26-f97e-11ea-8ad0-181deaf44bd2']
        ]

    
    for v in files:
        uuid_str = str(uuid.uuid1())
        print(uuid_str, v[0])
        pathFile = 'C:\\Users\\Oscar\\Documents\\cortex\\dataToUpload'
        fileService.uploadFile(pathFile, uuid_str, v[0])
    #fileService.downloadFile(localFolder, key, fileName)
    #fileService.deleteFile(key, fileName)
    